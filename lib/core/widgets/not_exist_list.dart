import 'package:flutter/material.dart';

class NotFound extends StatefulWidget {  
  final String name;
  final Function onTapCallback;
  final Icon icon;
  const NotFound({Key key,@required this.name, @required this.onTapCallback, this.icon}) : super(key: key);

  @override
  _NotFoundState createState() => _NotFoundState();
}

class _NotFoundState extends State<NotFound> {
  bool finishLoading = false;
  @override
  void initState() {
    super.initState();
     setState(() {
     _calculation();
       
     });
  }
  @override
  Widget build(BuildContext context) {
    return finishLoading?Container(
      color: Colors.grey[400],
      child: InkWell(
        onTap: ()=>widget.onTapCallback(),
            child: Center(
          child: Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(10),
            width: 250,
            height: 250,
            child: Center(
              child:Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("עוד לא הוספת\nאף ${widget.name},\n על מנת להוסיף \nלחץ כאן",style: TextStyle(color: Colors.white,fontSize: 17),textAlign: TextAlign.center,),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: widget.icon,
                  )
                ],
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(200),
              ),
              color: Colors.grey,
            ),
          ),
        ), 
      ),
    ):Center(child: CircularProgressIndicator());
}
  Future _calculation() {
    return Future.delayed(
      Duration(milliseconds: 500),
      () {
        setState(() {
          finishLoading=true;
        });
      }
    );
  }
  }