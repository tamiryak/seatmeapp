import 'package:get_it/get_it.dart';
import 'package:seatmeapp_finalproject/core/entities/user.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:seatmeapp_finalproject/core/entities/selected_guest.dart';
import 'package:seatmeapp_finalproject/core/selected_table.dart';


final getIt = GetIt.instance;

void setup() {
  getIt.registerSingleton(SeatMeAppUser());
  getIt.registerSingleton(SeatMeAppEvents());
  getIt.registerSingleton(SelectedTable());
  getIt.registerSingleton(SelectedGuest());
}
