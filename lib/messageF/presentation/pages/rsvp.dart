import 'package:flutter/material.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:seatmeapp_finalproject/core/entities/user.dart';

final SeatMeAppEvents currentEvent = getIt();
final SeatMeAppUser currentUser = getIt();

class Rsvp extends StatelessWidget {
  final List<String> recipents; //list of recipents fetched
  const Rsvp({Key key, @required this.recipents}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Theme.of(context).accentColor,
      padding: EdgeInsets.symmetric(vertical: 16),
      child: Text("שלח אישור הגעה",
          style: Theme.of(context).accentTextTheme.button),
      onPressed: () {
        var eventDate = currentEvent.eventDate; 
        var eventName = currentEvent.eventName;
        String eventLink = "https://seatmeapp.web.app/" + //creating the string to send
            currentUser.uid +
            "/" +
            currentEvent.eventId;
        String invite = "$eventNameהנך מוזמן ל" + "\n";
        String invite2 = "שתתקיים בתאריך $eventDate" + "\n";
        _sendSMS( 
            "שלום\n,$invite$invite2 לאישור הגעה אנא לחץ על הקישור \n $eventLink",
            recipents);
      },
    );
  }
}

//custom function to send sms to list of recipents
void _sendSMS(String message, List<String> recipents) async {
  String _result = await sendSMS(message: message, recipients: recipents)
      .catchError((onError) {
    print(onError);
  });
  print(_result);
}
