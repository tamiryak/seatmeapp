import 'package:flutter/material.dart';
import 'package:seatmeapp_finalproject/messageF/presentation/pages/confirmed_list.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';
import 'package:auto_route/auto_route.dart';
import 'package:seatmeapp_finalproject/messageF/presentation/pages/not_confirmed_list_page.dart';

class ConfirmationTabPage extends StatelessWidget {
  const ConfirmationTabPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //build of tab controller - contact/manual
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: true,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_sharp,
              color: Colors.black,
            ),
            onPressed: () {
              ExtendedNavigator.named(RoutesNames.mainNav).pop();
            },
          ),
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            "SeatMeApp",
            style: TextStyle(
                fontFamily: 'Lobster', fontSize: 25, color: Colors.black),
          ),

          bottom: TabBar(
            indicatorColor: Colors.grey,
            labelColor: Colors.black,
            unselectedLabelColor: Colors.grey,
            tabs: [
              Tab(
                icon: Icon(
                  Icons.verified_user,
                  color: Colors.black,
                ),
                text: 'מוזמנים שאישרו הגעה',
              ),
              Tab(
                icon: Icon(
                  Icons.approval,
                  color: Colors.black,
                ),
                text: 'מוזמנים שלא אישרו הגעה',
              ),
            ],
          ),
          // title: Text('Tabs Demo'),
        ),
        body: TabBarView(
          children: [
            ConfirmedListPage(), //run of tab pressed
            NotConfirmedListPage(),
          ],
        ),
      ),
    );
  }
}
