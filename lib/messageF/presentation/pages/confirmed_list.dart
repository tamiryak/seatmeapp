import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:seatmeapp_finalproject/core/entities/user.dart';
import 'package:seatmeapp_finalproject/guestsF/widgets/active_guests.dart';
import 'package:seatmeapp_finalproject/routes/routes.gr.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';

class ConfirmedListPage extends StatefulWidget {
  ConfirmedListPage({Key key}) : super(key: key);

  @override
  _ConfirmedListPageState createState() => _ConfirmedListPageState();
}

class _ConfirmedListPageState extends State<ConfirmedListPage> {
  final SeatMeAppEvents currentEvent = getIt();

  final SeatMeAppUser currentUser = getIt();

  final FirebaseFirestore firestoreInstance = FirebaseFirestore.instance;

  Stream stream;

  var firebaseUser = FirebaseAuth.instance.currentUser;

  @override
  void initState() {
    super.initState();
    stream = firestoreInstance //stream of confirmed list from firebase
        .collection("Users")
        .doc(currentUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId)
        .collection('Guests')
        .where('confirmed',isEqualTo:true )
        .snapshots();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: StreamBuilder(
        stream: stream,
        builder: (ctx, dataSnapshot) {
          if (dataSnapshot.connectionState == ConnectionState.waiting) { //initiate the stream builder
            return Center(child: CircularProgressIndicator());
          } else {
            if (dataSnapshot.error != null) {
              // ...
              // Do error handling stuff
              return Center(
                child: Text('An error occurred! please try again'),
              );
            }
            if (dataSnapshot.data.docs.length==0) {
              return Center(
                child: Text("אין עדיין אורחים שאישרו הגעה"), //text if list of contact conirmed empty
              );
            } else {
              var guests = dataSnapshot.data.docs;
              return new ListView( //create the list view with guest details
                  children: guests.map<Widget>((DocumentSnapshot document) {
                return new ActiveGuests(
                  document["guestName"],
                  document["guestType"],
                  document["guestPhone"],
                  document["guestEmail"],
                  document.id,
                  int.parse(document['numOfInvites'].toString()),
                );
              }).toList());
            }
          }
        },
      ),
    );
  }
}

func() {
  ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.guestTabPage);
}
