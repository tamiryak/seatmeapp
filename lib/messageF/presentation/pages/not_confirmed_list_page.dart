import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:seatmeapp_finalproject/core/entities/user.dart';
import 'package:seatmeapp_finalproject/guestsF/widgets/active_guests.dart';
import 'package:seatmeapp_finalproject/messageF/presentation/pages/rsvp.dart';
import 'package:seatmeapp_finalproject/routes/routes.gr.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';

List<String> phoneNumbers = [];

class NotConfirmedListPage extends StatefulWidget {
  NotConfirmedListPage({Key key}) : super(key: key);

  @override
  _NotConfirmedListPageState createState() => _NotConfirmedListPageState();
}

class _NotConfirmedListPageState extends State<NotConfirmedListPage> {
  final SeatMeAppEvents currentEvent = getIt();

  final SeatMeAppUser currentUser = getIt();

  final FirebaseFirestore firestoreInstance = FirebaseFirestore.instance;

  Stream stream;

  var firebaseUser = FirebaseAuth.instance.currentUser;
  @override
  void initState() {
    super.initState();
    stream = firestoreInstance //stream of not confirmed list of guest
        .collection("Users")
        .doc(currentUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId)
        .collection('Guests')
        .where('confirmed', isEqualTo: false)
        .snapshots();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          StreamBuilder(
            stream: stream,
            builder: (ctx, dataSnapshot) {
              if (dataSnapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else {
                if (dataSnapshot.error != null) {
                  // ...
                  // Do error handling stuff
                  return Center(
                    child: Text('An error occurred! please try again'),
                  );
                }
                if (dataSnapshot.data.docs.length==0) {
                  return Center(
                    child: Text("אין עדיין אורחים שאישרו הגעה"),
                  );
                } else {
                  var guests = dataSnapshot.data.docs;
                  phoneNumbers.clear();
                  guests.forEach((DocumentSnapshot guest) {
                    phoneNumbers.add(guest["guestPhone"]);
                  });
                  return new ListView(
                      children: guests.map<Widget>((DocumentSnapshot document) {
                    return new ActiveGuests(
                      document["guestName"],
                      document["guestType"],
                      document["guestPhone"],
                      document["guestEmail"],
                      document.id,
                      int.parse(document['numOfInvites'].toString()),
                    );
                  }).toList());
                }
              }
            },
          ),
          Positioned(
            bottom: 40,
            right: 30,
            child: Container(width: 130, child: Rsvp(recipents: phoneNumbers)), //button for sending message to guests
          ),
        ],
      ),
    );
  }
}

func() {
  ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.guestTabPage);
}
