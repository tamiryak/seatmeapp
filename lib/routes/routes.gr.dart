// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../core/entities/selected_guest.dart';
import '../guestsF/presentaion/pages/contacts_phone_page.dart';
import '../guestsF/presentaion/pages/guestTabPage.dart';
import '../guestsF/presentaion/pages/guest_list_page.dart';
import '../guestsF/presentaion/pages/guest_page.dart';
import '../guestsF/presentaion/pages/update_guest_page.dart';
import '../loginF/presentation/pages/home_page.dart';
import '../loginF/presentation/pages/intro_page.dart';
import '../loginF/presentation/pages/loadingF.dart';
import '../loginF/presentation/pages/login_page.dart';
import '../loginF/presentation/pages/new_event_page.dart';
import '../messageF/presentation/pages/confirmation_tab_page.dart';
import '../messageF/presentation/pages/rsvp.dart';
import '../seatsF/presentation/pages/active_event_page.dart';
import '../seatsF/presentation/pages/event_details_page.dart';
import '../seatsF/presentation/pages/seating_page.dart';
import '../seatsF/presentation/pages/table_seat_page.dart';

class Routes {
  static const String loginPage = '/';
  static const String homePage = '/home-page';
  static const String loading = '/Loading';
  static const String openEventPage = '/open-event-page';
  static const String seatingF = '/seating-f';
  static const String eventDetailsPage = '/event-details-page';
  static const String eventSelectionPage = '/event-selection-page';
  static const String guestPage = '/guest-page';
  static const String guestListPage = '/guest-list-page';
  static const String contactsPage = '/contacts-page';
  static const String guestTabPage = '/guest-tab-page';
  static const String rsvp = '/Rsvp';
  static const String confirmationTabPage = '/confirmation-tab-page';
  static const String intro = '/Intro';
  static const String tableSeatPage = '/table-seat-page';
  static const String updateGuestPage = '/update-guest-page';
  static const all = <String>{
    loginPage,
    homePage,
    loading,
    openEventPage,
    seatingF,
    eventDetailsPage,
    eventSelectionPage,
    guestPage,
    guestListPage,
    contactsPage,
    guestTabPage,
    rsvp,
    confirmationTabPage,
    intro,
    tableSeatPage,
    updateGuestPage,
  };
}

class SeatMeAppRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.loginPage, page: LoginPage),
    RouteDef(Routes.homePage, page: HomePage),
    RouteDef(Routes.loading, page: Loading),
    RouteDef(Routes.openEventPage, page: OpenEventPage),
    RouteDef(Routes.seatingF, page: SeatingF),
    RouteDef(Routes.eventDetailsPage, page: EventDetailsPage),
    RouteDef(Routes.eventSelectionPage, page: EventSelectionPage),
    RouteDef(Routes.guestPage, page: GuestPage),
    RouteDef(Routes.guestListPage, page: GuestListPage),
    RouteDef(Routes.contactsPage, page: ContactsPage),
    RouteDef(Routes.guestTabPage, page: GuestTabPage),
    RouteDef(Routes.rsvp, page: Rsvp),
    RouteDef(Routes.confirmationTabPage, page: ConfirmationTabPage),
    RouteDef(Routes.intro, page: Intro),
    RouteDef(Routes.tableSeatPage, page: TableSeatPage),
    RouteDef(Routes.updateGuestPage, page: UpdateGuestPage),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    LoginPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const LoginPage(),
        settings: data,
      );
    },
    HomePage: (data) {
      final args = data.getArgs<HomePageArguments>(
        orElse: () => HomePageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => HomePage(key: args.key),
        settings: data,
      );
    },
    Loading: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const Loading(),
        settings: data,
      );
    },
    OpenEventPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const OpenEventPage(),
        settings: data,
      );
    },
    SeatingF: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const SeatingF(),
        settings: data,
      );
    },
    EventDetailsPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const EventDetailsPage(),
        settings: data,
      );
    },
    EventSelectionPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const EventSelectionPage(),
        settings: data,
      );
    },
    GuestPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const GuestPage(),
        settings: data,
      );
    },
    GuestListPage: (data) {
      final args = data.getArgs<GuestListPageArguments>(
        orElse: () => GuestListPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => GuestListPage(key: args.key),
        settings: data,
      );
    },
    ContactsPage: (data) {
      final args = data.getArgs<ContactsPageArguments>(
        orElse: () => ContactsPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => ContactsPage(key: args.key),
        settings: data,
      );
    },
    GuestTabPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const GuestTabPage(),
        settings: data,
      );
    },
    Rsvp: (data) {
      final args = data.getArgs<RsvpArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) => Rsvp(
          key: args.key,
          recipents: args.recipents,
        ),
        settings: data,
      );
    },
    ConfirmationTabPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const ConfirmationTabPage(),
        settings: data,
      );
    },
    Intro: (data) {
      final args = data.getArgs<IntroArguments>(
        orElse: () => IntroArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => Intro(key: args.key),
        settings: data,
      );
    },
    TableSeatPage: (data) {
      final args = data.getArgs<TableSeatPageArguments>(
        orElse: () => TableSeatPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => TableSeatPage(
          key: args.key,
          tableKey: args.tableKey,
          isPlus: args.isPlus,
        ),
        settings: data,
      );
    },
    UpdateGuestPage: (data) {
      final args = data.getArgs<UpdateGuestPageArguments>(
        orElse: () => UpdateGuestPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => UpdateGuestPage(
          key: args.key,
          selectedGuest: args.selectedGuest,
        ),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// HomePage arguments holder class
class HomePageArguments {
  final Key key;
  HomePageArguments({this.key});
}

/// GuestListPage arguments holder class
class GuestListPageArguments {
  final Key key;
  GuestListPageArguments({this.key});
}

/// ContactsPage arguments holder class
class ContactsPageArguments {
  final Key key;
  ContactsPageArguments({this.key});
}

/// Rsvp arguments holder class
class RsvpArguments {
  final Key key;
  final List<String> recipents;
  RsvpArguments({this.key, @required this.recipents});
}

/// Intro arguments holder class
class IntroArguments {
  final Key key;
  IntroArguments({this.key});
}

/// TableSeatPage arguments holder class
class TableSeatPageArguments {
  final Key key;
  final String tableKey;
  final bool isPlus;
  TableSeatPageArguments({this.key, this.tableKey, this.isPlus});
}

/// UpdateGuestPage arguments holder class
class UpdateGuestPageArguments {
  final Key key;
  final SelectedGuest selectedGuest;
  UpdateGuestPageArguments({this.key, this.selectedGuest});
}
