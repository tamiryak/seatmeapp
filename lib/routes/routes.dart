import 'package:auto_route/auto_route_annotations.dart';
import 'package:seatmeapp_finalproject/guestsF/presentaion/pages/contacts_phone_page.dart';
import 'package:seatmeapp_finalproject/guestsF/presentaion/pages/guestTabPage.dart';
import 'package:seatmeapp_finalproject/guestsF/presentaion/pages/update_guest_page.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/pages/home_page.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/pages/intro_page.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/pages/loadingF.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/pages/login_page.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/pages/new_event_page.dart';
import 'package:seatmeapp_finalproject/messageF/presentation/pages/confirmation_tab_page.dart';
import 'package:seatmeapp_finalproject/messageF/presentation/pages/rsvp.dart';
import 'package:seatmeapp_finalproject/seatsF/presentation/pages/event_details_page.dart';
import 'package:seatmeapp_finalproject/guestsF/presentaion/pages/guest_page.dart';
import 'package:seatmeapp_finalproject/seatsF/presentation/pages/seating_page.dart';
import 'package:seatmeapp_finalproject/seatsF/presentation/pages/active_event_page.dart';
import 'package:seatmeapp_finalproject/guestsF/presentaion/pages/guest_list_page.dart';
import 'package:seatmeapp_finalproject/seatsF/presentation/pages/table_seat_page.dart';

// every change run in terminal flutter pub run build_runner build watch

@MaterialAutoRouter(
  routes: <AutoRoute>[
    MaterialRoute(page: LoginPage, initial: true),
    MaterialRoute(page: HomePage),
    MaterialRoute(page: Loading),
    MaterialRoute(page: OpenEventPage),
    MaterialRoute(page: SeatingF),
    MaterialRoute(page: EventDetailsPage),
    MaterialRoute(page: EventSelectionPage),
    MaterialRoute(page: GuestPage),
    MaterialRoute(page: GuestListPage),
    MaterialRoute(page: ContactsPage),
    MaterialRoute(page: GuestTabPage),
    MaterialRoute(page: Rsvp),
    MaterialRoute(page: ConfirmationTabPage),
    MaterialRoute(page: Intro),
    MaterialRoute(page:TableSeatPage),
    MaterialRoute(page:UpdateGuestPage)
  ],
)
class $SeatMeAppRouter {}
