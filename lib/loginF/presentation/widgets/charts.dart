import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';

int count;

class Chart_Guests extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => Chart_Guests2State();
}

class Chart_Guests2State extends State {
  int touchedIndex;
  final SeatMeAppEvents currentEvent = getIt();
  var firebaseUser = FirebaseAuth.instance.currentUser;
  final FirebaseFirestore firestoreInstance = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: firestoreInstance
            .collection("Users")
            .doc(firebaseUser.uid)
            .collection('Events')
            .doc(currentEvent.eventId)
            .collection('Guests')
            .snapshots(),
        builder: (ctx, dataSnapshot) {
          if (dataSnapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else {
            if (dataSnapshot.error != null) {
              // ...
              // Do error handling stuff
              return Center(
                child: Text('An error occurred! please try again'),
              );
            } else {
              return PieChart(
                PieChartData(
                    pieTouchData:
                        PieTouchData(touchCallback: (pieTouchResponse) {
                      setState(() {
                        if (pieTouchResponse.touchInput is FlLongPressEnd ||
                            pieTouchResponse.touchInput is FlPanEnd) {
                          touchedIndex = -1;
                        } else {
                          touchedIndex = pieTouchResponse.touchedSectionIndex;
                        }
                      });
                    }),
                    borderData: FlBorderData(
                      show: false,
                    ),
                    sectionsSpace: 0,
                    centerSpaceRadius: 40,
                    sections: showingSections(dataSnapshot.data.docs)),
              );
            }
          }
        });
  }

  List<PieChartSectionData> showingSections(var data) {
    double valueNotConfirm = 0;
    double valueConfirm = 0;
    for(int i=0;i<data.length;i++){
      var value=data[i];
      if (value['confirmed'] == true)
        {
          valueConfirm += 1;
        }
      else
        {
          valueNotConfirm += 1;
        }
    }


    // if (currentEvent.numOfConfirmed != null)
    //   value_confirm = currentEvent.numOfConfirmed.toDouble();
    // if (currentEvent.numOfNotConfirmed != null)
    //   double value_not_confirm = currentEvent.numOfNotConfirmed.toDouble();

    //     Provider.of<ChartsProvider>(context, listen: false)
    //         .num_of_not_confirmed();

    return List.generate(2, (i) {
      final isTouched = i == touchedIndex;
      final double fontSize = isTouched ? 25 : 13;
      final double radius = isTouched ? 60 : 50;
      final String confirmedPrecent = ((valueNotConfirm / (valueConfirm + valueNotConfirm)) * 100).toStringAsFixed(2);
      final String notConfirmedPrecent = ((valueConfirm / (valueConfirm + valueNotConfirm)) * 100).toStringAsFixed(2);
      switch (i) {
        case 0:
          return PieChartSectionData(
            color: const Color(0xffff1a1a),
            value:
                double.parse(confirmedPrecent),
            title: confirmedPrecent +
                "%",
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        case 1:
          return PieChartSectionData(
            color: const Color(0xff33cc33),
            value: double.parse(notConfirmedPrecent),
            title: notConfirmedPrecent +
                "%",
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        // case 2:
        //   return PieChartSectionData(
        //     color: const Color(0xff9e9e9e),
        //     value: 10,
        //     title: '10%',
        //     radius: radius,
        //     titleStyle: TextStyle(
        //         fontSize: fontSize,
        //         fontWeight: FontWeight.bold,
        //         color: const Color(0xffffffff)),
        //   );
        default:
          return null;
      }
    });
  }
}

class Indicator extends StatelessWidget {
  final Color color;
  final String text;
  final bool isSquare;
  final double size;
  final Color textColor;

  const Indicator({
    Key key,
    this.color,
    this.text,
    this.isSquare,
    this.size = 16,
    this.textColor = const Color(0xff505050),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Text(
          text,
          style: TextStyle(
              fontSize: 16, fontWeight: FontWeight.bold, color: textColor),
        ),
        const SizedBox(
          width: 4,
        ),
        Container(
          width: size,
          height: size,
          decoration: BoxDecoration(
            shape: isSquare ? BoxShape.rectangle : BoxShape.circle,
            color: color,
          ),
        ),
      ],
    );
  }
}
