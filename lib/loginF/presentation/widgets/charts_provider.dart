import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';

class ChartsProvider with ChangeNotifier {
  double count = 0;
  double count2 = 0;
  final FirebaseFirestore firestoreInstance;
  var firebaseUser = FirebaseAuth.instance.currentUser;
  final SeatMeAppEvents currentEvent;
  ChartsProvider()
      : firestoreInstance = FirebaseFirestore.instance,
        currentEvent = getIt();

 Future num_of_confirmed() async => firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId)
        .collection('Guests')
      .where('confirmed', isEqualTo: true)
      .get()
      .then((value) {
    var count = 0;
    count = value.docs.length;

    return count;
  });
 Future num_of_not_confirmed() async => firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId)
        .collection('Guests')
      .where('confirmed', isEqualTo: false)
      .get()
      .then((value) {
    var count = 0;
    count = value.docs.length;

    return count;
  });  
}

