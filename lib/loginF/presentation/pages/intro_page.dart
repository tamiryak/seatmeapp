import 'package:flutter/material.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';
import 'package:provider/provider.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/state_managment/new_event_provider.dart';

class Intro extends StatelessWidget {
 Intro({Key key}) : super(key: key);

final pages = [
    PageViewModel(
        pageColor: const Color(0xFF03A9F4),
        // iconImageAssetPath:'lib/core/assets/groom.png',
        body: Text(
          'SeatMeApp שלום וברוכים הבאים ל',
        ),
        title: Text(
          'ברוכים הבאים',
        ),
      titleTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
      bodyTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
        mainImage: Image.asset(
          'lib/core/assets/party.png',
          height: 300.0,
          width: 300.0,
          alignment: Alignment.center,
        )),
    PageViewModel(
      pageColor: const Color(0xFF8BC34A),
      // iconImageAssetPath: 'lib/core/assets/bride.png',
      body: Text(
        'אנחנו כאן על מנת לייצר את האירוע המושלם בקלות ובמהירות',
      ),
      title: Text('SeatMeApp'),
      mainImage: Image.asset(
        'lib/core/assets/calendar.png',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      titleTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
      bodyTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
    ),
    PageViewModel(
      pageColor: const Color(0xFF607D8B),
      // iconImageAssetPath: 'lib/core/assets/wedding.png',
      body: Text(
        'תחילה נקבל מכם טיפה פרטים על האירוע על מנת שנוכל ליצור לכם את הסביבה המתאימה',
      ),
      title: Center(child: Text('אירוע מותאם אישית')),
      mainImage: Image.asset(
        'lib/core/assets/seats.png',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      titleTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
      bodyTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'SeatMeApp', //title of app
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ), //ThemeData
      home: Builder(
        builder: (context) => IntroViewsFlutter(
          pages,
          showNextButton: true,
          showBackButton: true,
          onTapDoneButton: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) { Provider.of<NewEventPageProvider>(context, listen: false)
                  .introFinish();},
              ), //MaterialPageRoute
            );
          },
          pageButtonTextStyles: TextStyle(
            color: Colors.white,
            fontSize: 18.0,
          ),
        ), //IntroViewsFlutter
      ), //Builder
    ); //Material App
  }
}