import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/user.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/state_managment/drawer_notifier.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/state_managment/home_page_provider.dart';
import 'package:seatmeapp_finalproject/routes/routes.gr.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';

class LeftChild extends StatelessWidget {
  final SeatMeAppUser currentLoggedInUser = getIt();
  LeftChild({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("build left");

    return Consumer<DrawerNotifier>(
      builder: (context, drawer, child) {
        return Material(
            child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  // Add one stop for each color. Stops should increase from 0 to 1
                  //stops: [0.1, 0.5,0.5, 0.7, 0.9],
                  colors: [
                    ColorTween(
                      begin: Colors.red[200],
                      end: Colors.blueGrey[400].withRed(100),
                    ).lerp(drawer.swipeOffset),
                    ColorTween(
                      begin: Colors.redAccent[400],
                      end: Colors.blueGrey[800].withGreen(80),
                    ).lerp(drawer.swipeOffset),
                  ],
                ),
              ),
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 10, bottom: 15),
                              width: 80,
                              child: ClipRRect(
                                child: Image.network(
                                  "https://img.icons8.com/officel/2x/user.png",
                                ),
                                borderRadius: BorderRadius.circular(50),
                              ),
                            ),
                            Text(
                              currentLoggedInUser.name,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 18),
                            )
                          ],
                          //mainAxisAlignment: MainAxisAlignment.center,
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                        ),
                        ListTile(
                          onTap: () async {
                            await Navigator.of(context).pop();
                            Provider.of<HomePageProvider>(context,
                                    listen: false)
                                .navigateToEventPage();
                          },
                          title: Text(
                            "אירוע חדש",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                          leading: Icon(
                            Icons.dashboard,
                            color: Colors.white,
                            size: 22,
                          ),
                        ),
                        ListTile(
                          onTap: () async {
                            await Navigator.of(context).pop();
                            Provider.of<HomePageProvider>(context,
                                    listen: false)
                                .navigateToActiveEvents();
                          },
                          title: Text(
                            "האירועים שלי",
                            style: TextStyle(fontSize: 14, color: Colors.white),
                          ),
                          leading: Icon(
                            Icons.rounded_corner,
                            color: Colors.white,
                            size: 22,
                          ),
                        ),
                        ListTile(
                          onTap: () async {
                            await Navigator.of(context).pop();
                            ExtendedNavigator.named(RoutesNames.mainNav)
                                .push(Routes.guestListPage);
                          },
                          title: Text(
                            "רשימת המוזמנים",
                            style: TextStyle(fontSize: 14, color: Colors.white),
                          ),
                          leading:
                              Icon(Icons.list, size: 22, color: Colors.white),
                        ),
                        ListTile(
                          onTap: () async {
                            await Navigator.of(context).pop();
                            ExtendedNavigator.named(RoutesNames.mainNav)
                                .push(Routes.guestTabPage);
                          },
                          title: Text(
                            "הוספת אורחים",
                            style: TextStyle(fontSize: 14, color: Colors.white),
                          ),
                          leading: Icon(Icons.person_add,
                              size: 22, color: Colors.white),
                        ),
                        ListTile(
                          onTap: () async {
                            await Navigator.of(context).pop();
                            ExtendedNavigator.named(RoutesNames.mainNav)
                                .push(Routes.confirmationTabPage);
                          },
                          title: Text(
                            "אישורי הגעה",
                            style: TextStyle(fontSize: 14, color: Colors.white),
                          ),
                          leading: Icon(Icons.bookmark_border,
                              size: 22, color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    child: Container(
                      alignment: Alignment.bottomLeft,
                      margin: EdgeInsets.only(top: 50),
                      padding:
                          EdgeInsets.symmetric(vertical: 15, horizontal: 25),
                      width: double.maxFinite,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            Icons.logout,
                            size: 18,
                            color: Colors.white,
                          ),
                          GestureDetector(
                            onTap: () async {
                              await Navigator.of(context).pop();
                              Provider.of<HomePageProvider>(context,
                                      listen: false)
                                  .signOutGoogle();
                              Provider.of<HomePageProvider>(context,
                                      listen: false)
                                  .navigateToLogout();
                              currentLoggedInUser.name = '';
                            },
                            child: Text(
                              " התנתק",
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            // drawer.swipeOffset < 1
            //     ? BackdropFilter(
            //         filter: ImageFilter.blur(
            //             sigmaX: (10 - drawer.swipeOffset * 10),
            //             sigmaY: (10 - drawer.swipeOffset * 10)),
            //         child: Container(
            //           decoration: BoxDecoration(
            //             color: Colors.black.withOpacity(0),
            //           ),
            //         ),
            //       )
            //     : null,
          ].where((a) => a != null).toList(),
        ));
      },
    );
  }
}
