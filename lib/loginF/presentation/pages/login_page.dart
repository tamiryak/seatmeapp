import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:provider/provider.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/state_managment/login_provider.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key key}) : super(key: key);

  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.55,
            width: MediaQuery.of(context).size.width,
            color: Colors.red,
            child: Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.15,
                ),
                Text(
                  "SeatMeApp",
                  style: TextStyle(
                      color: Colors.white, fontSize: 25, fontFamily: 'Lobster'),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: MediaQuery.of(context).size.height * 0.2,
                  height: MediaQuery.of(context).size.height * 0.2,
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      color: Colors.white, shape: BoxShape.circle),
                  child: Image.asset("lib/core/assets/logo.png"),
                ),
              ],
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 30,
              ),
              Text("ברוכים הבאים",
                  style: TextStyle(
                      color: Colors.redAccent,
                      fontSize: MediaQuery.of(context).size.width * 0.08,
                      fontWeight: FontWeight.bold)),
              SizedBox(
                height: 20,
              ),
              ButtonLogin(text: "התחבר עם גוגל"),
              SizedBox(
                height: 10,
              ),
              FaceBookButton(text: "התחבר עם פייסבוק"),
              SizedBox(
                height: 10,
              ),

              //PhoneLogin(text: "התחבר באמצעות מס טלפון"),
              SizedBox(
                height: 10,
              ),
              // InkWell(
              //     onTap: () {},
              //     child: Text(
              //       'לא נרשמת עדיין? לחץ כאן',
              //       style: TextStyle(
              //           color: Colors.blue,
              //           decoration: TextDecoration.underline),
              //     ))
            ],
          ),
        ],
      ),
    );
  }
}

class ButtonLogin extends StatelessWidget {
  final String text;
  ButtonLogin({
    Key key,
    @required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SignInButton(
      Buttons.Google,
      padding: const EdgeInsets.symmetric(vertical: 5),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30),
          side: BorderSide(color: Colors.black)),
      elevation: 5,
      text: text,
      onPressed: () async {
        await Provider.of<LoginProvider>(context, listen: false)
            .signInWithGoogle();
      },
    );
  }
}

class FaceBookButton extends StatelessWidget {
  final String text;
  FaceBookButton({
    Key key,
    @required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SignInButton(
      Buttons.Facebook,
      onPressed: () {},
      padding: const EdgeInsets.symmetric(vertical: 13),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30),
          side: BorderSide(color: Colors.black)),
      elevation: 5,
      text: text,
    );
  }
}

class PhoneLogin extends StatelessWidget {
  final String text;
  PhoneLogin({
    Key key,
    @required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SignInButton(
      Buttons.Email,
      onPressed: () {},
      padding: const EdgeInsets.symmetric(vertical: 13),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30),
          side: BorderSide(color: Colors.black)),
      elevation: 5,
      text: text,
    );
  }
}
