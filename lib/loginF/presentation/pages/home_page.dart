import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';
import 'package:provider/provider.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:seatmeapp_finalproject/core/entities/user.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/state_managment/home_page_provider.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/widgets/carousel_widget.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/widgets/charts.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/pages/left_child.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/state_managment/drawer_notifier.dart';
import 'package:seatmeapp_finalproject/seatsF/presentation/state_managment/active_events_provider.dart';

class HomePage extends StatefulWidget {
  HomePage({
    Key key,
  }) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

final FirebaseFirestore firestoreInstance = FirebaseFirestore.instance;

class _HomePageState extends State<HomePage> {
  final SeatMeAppUser currentLoggedInUser = getIt();
  final SeatMeAppEvents currentEvent = getIt();
  var firebaseUser = FirebaseAuth.instance.currentUser;

  @override
  Widget build(BuildContext context) {
    final drawerNotifier = Provider.of<DrawerNotifier>(context, listen: true);
    return SafeArea(
      child: InnerDrawer(
        key: _innerDrawerKey,
        onTapClose: drawerNotifier.onTapToClose,
        tapScaffoldEnabled: drawerNotifier.tapScaffold,
        velocity: 20,
        //swipeChild: true,
        offset: IDOffset.horizontal(drawerNotifier.offset),
        swipe: drawerNotifier.swipe,
        colorTransitionChild: drawerNotifier.colorTransition,
        colorTransitionScaffold: drawerNotifier.colorTransition,
        leftAnimationType: drawerNotifier.animationType,
        rightAnimationType: InnerDrawerAnimation
            .linear, // default  Theme.of(context).backgroundColor

        //when a pointer that is in contact with the screen and moves to the right or left
        onDragUpdate: (double val, InnerDrawerDirection direction) {
          // return values between 1 and 0
          print(val);
          // check if the swipe is to the right or to the left
          print(direction == InnerDrawerDirection.start);
        },

        innerDrawerCallback: (a) =>
            print(a), // return  true (open) or false (close)
        leftChild: LeftChild(),
        // required if rightChild is not set
        // rightChild: Container(), // required if leftChild is not set

        scaffold: Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            elevation: 10,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(40),
              ),
            ),
            // leading: Container(
            //   width: 10,
            //   height: 10,
            //   child:
            //   Row(
            //     crossAxisAlignment: CrossAxisAlignment.stretch,
            //     children: [Image.asset("lib/core/assets/logo2.png",fit: BoxFit.contain,)]
            //   ),
            // ),
            title: Text(
              'SeatMeApp',
              style: TextStyle(fontFamily: 'Lobster', color: Colors.black),
            ),
            actions: [
              IconButton(
                  icon: Icon(Icons.notifications_outlined),
                  color: Colors.black,
                  onPressed: null)
            ],
            centerTitle: true,
            backgroundColor: Colors.white,
          ),
          body: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Text("ברוכים הבאים",
                        style: TextStyle(
                            color: Colors.black38,
                            fontSize: MediaQuery.of(context).size.height * 0.04,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Lobster')),

                    StreamBuilder(
                        stream: firestoreInstance
                            .collection("Users")
                            .doc(firebaseUser.uid)
                            .collection('Events')
                            .doc(currentEvent.eventId)
                            .snapshots(),
                        builder: (ctx, dataSnapshot) {
                          if (dataSnapshot.connectionState ==
                              ConnectionState.waiting) {
                            return Center(child: CircularProgressIndicator());
                          } else {
                            if (dataSnapshot.error != null) {
                              // ...
                              // Do error handling stuff
                              return Center(
                                child:
                                    Text('An error occurred! please try again'),
                              );
                            } else {
                              return Text('${currentEvent.eventName??""}',
                                  style: TextStyle(
                                      color: Colors.black45,
                                      fontSize:
                                          MediaQuery.of(context).size.height *
                                              0.04,
                                      fontWeight: FontWeight.bold));
                            }
                          }
                        }),

                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      child: CarouselWithIndicator(),
                      width: MediaQuery.of(context).size.width,
                      height: 230,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(color: Colors.black, width: 3.0),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(20),
                          child: Container(
                            width: 140,
                            height: 140,
                            child: FloatingActionButton(
                                onPressed: () {
                                  Provider.of<HomePageProvider>(context,
                                          listen: false)
                                      .navigateToSeatingF();
                                },
                                backgroundColor: Colors.red[400],
                                elevation: 3,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Container(
                                  child: Container(
                                    decoration: new BoxDecoration(
                                      image: new DecorationImage(
                                        image: new ExactAssetImage(
                                            'lib/core/assets/party.jpg'),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    child: BackdropFilter(
                                      filter: ImageFilter.blur(
                                          sigmaX: 3, sigmaY: 2),
                                      child: Center(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Text(
                                              'לתצוגת \nהאולם',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 23,
                                              ),
                                            ),
                                            Icon(Icons.arrow_forward)
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                )),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      width: 200,
                      height: 240,
                      child: StreamBuilder(
                          stream: firestoreInstance
                              .collection("Users")
                              .doc(firebaseUser.uid)
                              .collection('Events')
                              .doc(currentEvent.eventId)
                              .snapshots(),
                          builder: (ctx, dataSnapshot) {
                            if (dataSnapshot.connectionState ==
                                ConnectionState.waiting) {
                              return Center(child: CircularProgressIndicator());
                            } else {
                              if (dataSnapshot.error != null) {
                                // ...
                                // Do error handling stuff
                                return Center(
                                  child: Text(
                                      'An error occurred! please try again'),
                                );
                              } else {
                                return Chart_Guests();
                              }
                            }
                          }),
                    ),
                    // SizedBox(height: 10,),
                    Row(
                      children: [
                        Indicator(
                          text: "לא מגיעים",
                          color: Color(0xff737373),
                          isSquare: true,
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Indicator(
                          text: "לא אישרו",
                          color: Color(0xffe32222),
                          isSquare: true,
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Indicator(
                          text: "אישרו",
                          color: Color(0xff32a852),
                          isSquare: true,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<bool> fetchData() => Future.delayed(Duration(seconds: 1), () {
        Provider.of<EventListProvider>(context, listen: false)
            .getCountOfConfirmations();
        return true;
      });
}

final GlobalKey<InnerDrawerState> _innerDrawerKey =
    GlobalKey<InnerDrawerState>();

void _toggle() {
  _innerDrawerKey.currentState.toggle(
      // direction is optional
      // if not set, the last direction will be used
      //InnerDrawerDirection.start OR InnerDrawerDirection.end
      direction: InnerDrawerDirection.end);
}
