import 'package:flutter/material.dart';

class Loading extends StatelessWidget {
  const Loading({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Center(
            child:
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                  Image.asset(
                    "lib/core/assets/loading.gif",
                    height: MediaQuery.of(context).size.height*0.86,
                    width: MediaQuery.of(context).size.width*0.86
                  )

              ],
        ),
      ),
    );
  }
}
