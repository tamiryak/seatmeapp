import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/state_managment/new_event_provider.dart';

import 'package:flutter/cupertino.dart';

class OpenEventPage extends StatelessWidget {
  const OpenEventPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: Provider.of<NewEventPageProvider>(context, listen: false).fbKey,
      initialValue: {},
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_sharp,
              color: Colors.black,
            ),
            onPressed: () {
              Provider.of<NewEventPageProvider>(context, listen: false)
                  .navigateToHomePage();
            },
          ),
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            "SeatMeApp",
            style: TextStyle(
                fontFamily: 'Lobster', fontSize: 18, color: Colors.black),
          ),
        ),
        body: Container(
          child: Column(
            children: [
              SizedBox(
                height: 50,
              ),
              TextFieldsEditor(),
            ],
          ),
        ),
      ),
    );
  }
}

class TextFieldsEditor extends StatefulWidget {
  TextFieldsEditor({Key key}) : super(key: key);

  @override
  _TextFieldsEditorState createState() => _TextFieldsEditorState();
}

class _TextFieldsEditorState extends State<TextFieldsEditor> {
  double sliderValue = 10.0;
  DateTime myDate = new DateTime(
      DateTime.now().year, DateTime.now().month, DateTime.now().day);
  @override
  Widget build(BuildContext context) {
    var now = DateTime.now();
    var today = new DateTime(now.year, now.month, now.day);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          Text(
            "בואו נתחיל ביצירת האירוע",
            style: TextStyle(
              fontFamily: 'Lobster',
              fontSize: 30,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          EventTextField(
            name: "eventName",
            lableText: 'שם אירוע',
            digitOnly: false,
          ),
          SizedBox(
            height: 20,
          ),
          Text(":תאריך האירוע"),
          SizedBox(
            height: 20,
          ),
          Container(
            height: 50,
            child: CupertinoDatePicker(
              minimumDate: today,
              mode: CupertinoDatePickerMode.date,
              //initialDateTime: DateTime(2020, 12, 31),
              onDateTimeChanged: (DateTime newDateTime) {
                myDate = newDateTime;
              },
            ),
          ),
          SizedBox(
            height: 20,
          ),
          EventTextField(
              name: "numOfGuests", lableText: "מספר מוזמנים", digitOnly: true),
          SizedBox(
            height: 20,
          ),
          Text("מספר אנשים בשולחן"),
          Slider(
            activeColor: Colors.deepOrange,
            min: 10.0,
            max: 12.0,
            divisions: 1,
            onChanged: (newRating) {
              setState(() {
                sliderValue = newRating;
              });
            },
            value: sliderValue,
          ),
          Text("${sliderValue.round()}"),
          SizedBox(
            height: 20,
          ),
          FloatingActionButton(
            elevation: 5,
            backgroundColor: Colors.deepOrange,
            child: Container(
                width: 100,
                height: 100,
                child: Center(
                    child: Text(
                  "!צור אירוע",
                  textAlign: TextAlign.center,
                ))),
            onPressed: () async {
              if(Provider.of<NewEventPageProvider>(context, listen: false).fbKey.currentState.validate()){
              await Provider.of<NewEventPageProvider>(context, listen: false)
                  .registerComplete(sliderValue, myDate);
              }
            },
          )
        ],
      ),
    );
  }
}

class EventTextField extends StatelessWidget {
  final String name;
  final String lableText;
  final bool multiline;
  final bool digitOnly;
  const EventTextField(
      {Key key,
      this.multiline = false,
      @required this.name,
      this.lableText,
      @required this.digitOnly})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: FormBuilderTextField(
            inputFormatters: [
              if (digitOnly) FilteringTextInputFormatter.digitsOnly
              // else
              //   FilteringTextInputFormatter.allow(
              //     RegExp("[a-zA-Z0-9]"),
              //   ),
              // LengthLimitingTextInputFormatter(4),
            ],
            validators: [
              FormBuilderValidators.required(),
              
              //digitOnly?FormBuilderValidators.numeric():null,

            ],
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: lableText,
            ),
            attribute: name,
          ),
        ),
      ],
    );
  }
}

class DateBox extends StatefulWidget {
  final String attribute;
  DateBox({Key key, this.attribute}) : super(key: key);

  @override
  _DateBoxState createState() => _DateBoxState();
}

class _DateBoxState extends State<DateBox> {
  @override
  Widget build(BuildContext context) {
    // var now = DateTime.now();
    // var today = new DateTime(now.year, now.month, now.day);
    return Container(
      height: 50,
      child: CupertinoDatePicker(
        // minimumDate: today,
        mode: CupertinoDatePickerMode.date,
        //initialDateTime: DateTime(2020, 12, 31),
        onDateTimeChanged: (DateTime newDateTime) {
          // Do something
        },
      ),
    );
  }
}

class PhotoUploader extends StatelessWidget {
  const PhotoUploader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black12,
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 10),
          CirclePhoto(),
          SizedBox(height: 10),
          InkWell(
            child:
                Text('Upload User Photo', style: TextStyle(color: Colors.blue)),
            onTap: () => {},
          ),
          SizedBox(height: 10),
        ],
      ),
    );
  }
}

class CirclePhoto extends StatelessWidget {
  final String url;
  const CirclePhoto({Key key, this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (url == null) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(1000),
        child: Container(
          height: MediaQuery.of(context).size.width * 0.4,
          width: MediaQuery.of(context).size.width * 0.4,
          color: Colors.black38,
          child: Center(
            child: Icon(
              Icons.add,
              size: 50,
              color: Colors.white,
            ),
          ),
        ),
      );
    } else {
      return Image(
        image: NetworkImage(url),
      );
    }
  }
}
