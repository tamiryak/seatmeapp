import 'package:auto_route/auto_route.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:seatmeapp_finalproject/core/entities/user.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/loginF/core/login_failure.dart';
import 'package:seatmeapp_finalproject/loginF/core/shared_preferences_names.dart';
import 'package:seatmeapp_finalproject/routes/routes.gr.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class LoginProvider with ChangeNotifier {
  final FirebaseAuth _auth;
  final GoogleSignIn googleSignIn;
  final SeatMeAppUser currentLoggedInUser;
  var firebaseUser = FirebaseAuth.instance.currentUser;
  final SeatMeAppEvents currentEvent;
  final firestoreInstance;

  LoginProvider()
      : _auth = FirebaseAuth.instance,
        googleSignIn = GoogleSignIn(),
        currentLoggedInUser = getIt(),
        currentEvent = getIt(),
        firestoreInstance = FirebaseFirestore.instance;

  Future<Either<SeatMeAppUser, LoginFailure>> _signInWithGoogle() async {
    await Firebase.initializeApp();

    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    UserCredential authResult;
    User user;
    if (_auth.currentUser == null) {
      authResult = await _auth.signInWithCredential(credential);
      user = authResult.user;
    } else {
      user = _auth.currentUser;
    }

    if (user != null) {
      assert(!user.isAnonymous);
      assert(await user.getIdToken() != null);

      final User currentUser = _auth.currentUser;
      assert(user.uid == currentUser.uid);

      currentLoggedInUser.uid = user.uid;
      currentLoggedInUser.name = user.displayName;
      currentLoggedInUser.email = user.email;

      print('signInWithGoogle succeeded: $user');

      return Left(currentLoggedInUser);
    }

    return Right(LoginFailure());
  }

  Future signInWithGoogle() async {
    Either<SeatMeAppUser, LoginFailure> result = await _signInWithGoogle();

    result.fold(
      (SeatMeAppUser user) async {
        SharedPreferences prefs = await SharedPreferences.getInstance();

        if (prefs.getBool(IS_REGISTERED) == null) {
          var firebaseUser = FirebaseAuth.instance.currentUser;
          firestoreInstance.collection("Users").doc(firebaseUser.uid).set({
            "name": currentLoggedInUser.name,
            "email": currentLoggedInUser.email,
          }).then((_) {
            print("success!");
          });
          navigateToIntro();
          //navigateToNewEventPage();
          prefs.setBool(IS_REGISTERED, true);
        } else {
          navigateToHomePage();
        }
        setActiveCurrentEvent();
      },
      (LoginFailure) {
        print("Login Failed");
      },
    );
  }

  Future<void> setActiveCurrentEvent() async {
    var event = firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .where('active', isEqualTo: true);

    await event.get().then((eventDetails) {
      if (eventDetails.docs.length > 0) {
        currentEvent.eventName = eventDetails.docs[0].get('EventName');
        currentEvent.eventDate = eventDetails.docs[0].get('eventDate');
        currentEvent.eventId = eventDetails.docs[0].id;
      }
    });
  }

  void signOutGoogle() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('IS_REGISTERD', true);
    await googleSignIn.signOut();
    navigateToLogout();
    print("User Signed Out");
  }

  void navigateToEventPage() {
    ExtendedNavigator.named(RoutesNames.mainNav)
        .popAndPush(Routes.openEventPage);
  }

  void navigateToHomePage() {
    ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.homePage);
  }

  void navigateToLogout() {
    ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.loginPage);
  }

  void checkIfConnected() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getBool(IS_REGISTERED) != null) {
      navigateToHomePage();
    }
  }

  void navigateToNewEventPage() {
    ExtendedNavigator.named(RoutesNames.mainNav)
        .popAndPush(Routes.openEventPage);
  }

  void navigateToIntro() {
    ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.intro);
  }
}
