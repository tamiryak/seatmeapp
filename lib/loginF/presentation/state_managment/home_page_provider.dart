import 'package:auto_route/auto_route.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:seatmeapp_finalproject/routes/routes.gr.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';
import 'dart:math';


class HomePageProvider with ChangeNotifier {
  final GoogleSignIn googleSignIn;
  final FirebaseAuth _auth;

  HomePageProvider()
      : _auth = FirebaseAuth.instance,
        googleSignIn = GoogleSignIn();

  void signOutGoogle() async {
    await googleSignIn.signOut();
    navigateToLogout();
    print("User Signed Out");
  }

  void navigateToEventPage() {
    ExtendedNavigator.named(RoutesNames.mainNav)
        .push(Routes.openEventPage);
  }

  void navigateToLoadingPage()  {
    ExtendedNavigator.named(RoutesNames.mainNav).push(Routes.loading);
    navigateToHomePage();
  }
  void navigateToHomePage() {
    ExtendedNavigator.named(RoutesNames.mainNav).push(Routes.homePage);
  }

  void navigateToLogout() {
    ExtendedNavigator.named(RoutesNames.mainNav).push(Routes.loginPage);
  }

  void navigateToActiveEvents() {
    ExtendedNavigator.named(RoutesNames.mainNav)
        .push(Routes.eventSelectionPage);
  }
    int random(min, max) {
    var rn = new Random();
    return min + rn.nextInt(max - min);
  }


  void navigateToSeatingF() async {
  ExtendedNavigator.named(RoutesNames.mainNav).push(Routes.seatingF);
}

}
