import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/user.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:seatmeapp_finalproject/loginF/core/shared_preferences_names.dart';
import 'package:seatmeapp_finalproject/routes/routes.gr.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:math';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NewEventPageProvider with ChangeNotifier {
  final SeatMeAppUser currentLoggedInUser;
  final SeatMeAppEvents currentEvent;
  final firestoreInstance = FirebaseFirestore.instance;

  final GlobalKey<FormBuilderState> fbKey = GlobalKey<FormBuilderState>();

  NewEventPageProvider()
      : currentLoggedInUser = getIt(),
        currentEvent = getIt();

  Future<void> registerComplete(double sliderVal, DateTime myDate) async {
    if (fbKey.currentState.saveAndValidate()) {
      print(fbKey.currentState.value);
      // await saveEventOnFireStore(currentLoggedInUser);
      currentLoggedInUser.email = fbKey.currentState.value['Email'];
      currentEvent.eventName = fbKey.currentState.value['eventName'];
      currentEvent.numberOfGuests =
          int.parse(fbKey.currentState.value['numOfGuests']);
      currentEvent.numOfChairsOnTable = sliderVal.round();
      DateFormat format = DateFormat("dd/MM/yyyy hh:mm");
      var outputDate = format.format(myDate);
      currentEvent.eventDate = outputDate;

      navigateToLoadingPage(currentEvent);
    }

    //currentEvent.eventName = fbKey.currentState.value['eventDate'];
  }

  void navigateToLoadingPage(SeatMeAppEvents currentEvent) async {
    ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.loading);
    await _calculation;
    await saveEventOnFireStore(currentEvent);
    //await ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.seatingF);
    ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.homePage);
  }

  Future saveEventOnFireStore(SeatMeAppEvents eventToSave) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(HAVE_TABLES, 1);
    var firebaseUser = FirebaseAuth.instance.currentUser;
    CollectionReference events = firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events');
    final docRef = events.doc();
    docRef
        .set({
          'EventName': eventToSave.eventName,
          'eventDate': eventToSave.eventDate,
          'numberOfGuests': eventToSave.numberOfGuests,
          'numOfChairsOnTable': eventToSave.numOfChairsOnTable,
          'active': true,
          'relations': ['עבודה', 'חברים', 'משפחה', 'הוספת קטגוריות']
        })
        .then((value) => value)
        .catchError((error) => print("Failed to add user: $error"));
    currentEvent.eventId = docRef.id;
  }

  void navigateToHomePage() {
    ExtendedNavigator.named(RoutesNames.mainNav).pop();
  }

  void introFinish() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(IS_REGISTERED, true);
    navigateToNewEventPage();
  }

  void navigateToNewEventPage() {
    ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.openEventPage);
  }
}

Future<String> _calculation = Future<String>.delayed(
  Duration(seconds: random(2, 10)),
  () => 'Data Loaded',
);

int random(min, max) {
  var rn = new Random();
  return min + rn.nextInt(max - min);
}
