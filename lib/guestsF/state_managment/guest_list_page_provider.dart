import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/selected_guest.dart';
import 'package:seatmeapp_finalproject/routes/routes.gr.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';

class GuestListPageProvider with ChangeNotifier {
  final FirebaseFirestore firestoreInstance;
  var firebaseUser = FirebaseAuth.instance.currentUser;
  final SeatMeAppEvents currentEvent;

  GuestListPageProvider()
      : firestoreInstance = FirebaseFirestore.instance,
        currentEvent = getIt();

  List<SelectedGuest> _items = [];

  List<SelectedGuest> get items {
    return [..._items];
  }

  void addItem(SelectedGuest guest) {
    _items.insert(itemCount, guest);
    notifyListeners();
  }

  // double get totalAmount {
  //   var total = 0.0;
  //   _items.forEach((key, cartItem) {
  //     total += cartItem.price;
  //   });
  //   return total;
  // }

  int get itemCount {
    return _items.length;
  }

  // remove item from cart
  void removeItem(String guestId) {
    _items.forEach((element) {
      if (element.guestKey == guestId) {
        _items.remove(element);
      }
    });
    notifyListeners();
  }

  void clear() {
    _items = [];
    notifyListeners();
  }


  void navigateToHomePage() {
    ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.homePage);
  }

  void navigateToEventDetailsPage() {
    ExtendedNavigator.named(RoutesNames.mainNav)
        .popAndPush(Routes.eventDetailsPage);
  }

  void navigateToAddGuestPage() {
    ExtendedNavigator.named(RoutesNames.mainNav)
        .push(Routes.guestTabPage);
  }

  Future<void> createConfirmedGuestList() async {
    clear();
    CollectionReference guests = firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId)
        .collection('Guests');

    guests
        .where('confirmed', isEqualTo: true)
        .get()
        .then((value) => value.docs.forEach((element) {
              SelectedGuest guest = SelectedGuest();

              guest.guestKey = element.id;
              guest.guestName = element.get('guestName');
              guest.guestType = element.get('guestType');
              guest.guestPhone = element.get('guestPhone');
              guest.guestEmail = element.get('guestEmail');
              guest.numOfInvites = element.get('numOfInvites');
              guest.confirmed = element.get('confirmed');

              // event.eventDate = DateTime.parse(element.get('eventDate').toString());
              addItem(guest);
            }));
    notifyListeners();
  }

  Future<void> createNotConfirmedGuestList() async {
    clear();
    CollectionReference guests = firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId)
        .collection('Guests');

    guests.get().then((value) => value.docs.forEach((element) {
          SelectedGuest guest = SelectedGuest();
          if (!element.get('confirmed')) {
            guest.guestKey = element.id;
            guest.guestName = element.get('guestName');
            guest.guestType = element.get('guestType');
            guest.guestPhone = element.get('guestPhone');
            guest.guestEmail = element.get('guestEmail');
            guest.numOfInvites = element.get('numOfInvites');
            guest.confirmed = element.get('confirmed');
          }

          // event.eventDate = DateTime.parse(element.get('eventDate').toString());
          addItem(guest);
        }));
    notifyListeners();
  }
}
