import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:seatmeapp_finalproject/core/entities/selected_guest.dart';
import 'package:seatmeapp_finalproject/core/entities/user.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';

class UpdateGuestProvider with ChangeNotifier {
  final SeatMeAppUser currentLoggedInUser;
  final SeatMeAppEvents currentEvent;
  final firestoreInstance = FirebaseFirestore.instance;

  final GlobalKey<FormBuilderState> fbKey = GlobalKey<FormBuilderState>();

  final GlobalKey<FormBuilderState> fbKey2 = GlobalKey<FormBuilderState>();
  UpdateGuestProvider()
      : currentLoggedInUser = getIt(),
        currentEvent = getIt();

  Future<void> guestUpdate(SelectedGuest selectedGuest) async {
    if (fbKey.currentState.saveAndValidate()) {
      print(fbKey.currentState.value);
      selectedGuest.guestName = fbKey.currentState.value['guestName'];
      selectedGuest.guestEmail = fbKey.currentState.value['guestEmail'];
      selectedGuest.guestPhone = fbKey.currentState.value['guestPhone'];
      selectedGuest.numOfInvites =
          int.parse(fbKey.currentState.value['numOfInvites']);
      updateGuestOnFireStore(selectedGuest);
    }
  }

  Future updateGuestOnFireStore(SelectedGuest guestToUpate) async {
    var firebaseUser = FirebaseAuth.instance.currentUser;
    DocumentReference guest = firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId)
        .collection('Guests')
        .doc(guestToUpate.guestKey);
    guest
        .update({
          'guestName': guestToUpate.guestName ?? "",
          'guestType': guestToUpate.guestType ?? "",
          'guestPhone': guestToUpate.guestPhone ?? "",
          'guestEmail': guestToUpate.guestEmail ?? "",
          'numOfInvites': guestToUpate.numOfInvites ?? 1,
        })
        .then((value) => value)
        .catchError((error) => print("Failed to update user: $error"));
  }

  void navigateToLastPage() {
    ExtendedNavigator.named(RoutesNames.mainNav).pop();
  }
}
