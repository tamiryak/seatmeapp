import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/user.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:seatmeapp_finalproject/core/entities/selected_guest.dart';
import 'package:seatmeapp_finalproject/loginF/core/shared_preferences_names.dart';
import 'package:seatmeapp_finalproject/routes/routes.gr.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:math';

import 'package:shared_preferences/shared_preferences.dart';

class GuestPageProvider with ChangeNotifier {
  final SeatMeAppUser currentLoggedInUser;
  final SeatMeAppEvents currentEvent;
  final SelectedGuest currentGuest;
  final firestoreInstance = FirebaseFirestore.instance;

  final GlobalKey<FormBuilderState> fbKey = GlobalKey<FormBuilderState>();

  final GlobalKey<FormBuilderState> fbKey2 = GlobalKey<FormBuilderState>();
  List<String> items = [];
  GuestPageProvider()
      : currentLoggedInUser = getIt(),
        currentGuest = getIt(),
        currentEvent = getIt();

  Future<void> guestComplete(String guest_type) async {
    if (fbKey.currentState.saveAndValidate()) {
      print(fbKey.currentState.value);
      // await saveEventOnFireStore(currentLoggedInUser);
      currentLoggedInUser.email = fbKey.currentState.value['Email'];
      currentGuest.guestName = fbKey.currentState.value['guestName'];
      currentGuest.guestType = guest_type;
      currentGuest.guestEmail = fbKey.currentState.value['guestEmail'];
      currentGuest.guestPhone = fbKey.currentState.value['guestPhone'];
      currentGuest.numOfInvites =
          int.parse(fbKey.currentState.value['numOfInvites']);
      currentGuest.confirmed = false;

      navigateToLoadingPage(currentGuest);
    }

    //currentEvent.eventName = fbKey.currentState.value['eventDate'];

    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(IS_REGISTERED, true);
  }

  void navigateToLoadingPage(SelectedGuest currentGuest) async {
    //  SharedPreferences prefs = await SharedPreferences.getInstance();
    // await prefs.setInt(HAVE_TABLES, 1);
    //ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.loading);
    // await _calculation;
    await saveGuestOnFireStore(currentGuest).timeout(Duration(seconds: 20));
    fbKey.currentState.reset();
    // REFRESH
  }

  Future saveGuestOnFireStore(SelectedGuest guestToSave) async {
    var firebaseUser = FirebaseAuth.instance.currentUser;
    CollectionReference guests = firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId)
        .collection('Guests');
    final docRef = guests.doc();
    docRef
        .set({
          'guestName': guestToSave.guestName ?? "",
          'guestType': guestToSave.guestType ?? "",
          'guestPhone': guestToSave.guestPhone ?? "",
          'guestEmail': guestToSave.guestEmail ?? "",
          'numOfInvites': guestToSave.numOfInvites ?? 1,
          'confirmed': guestToSave.confirmed
        })
        .then((value) => value)
        .catchError((error) => print("Failed to add user: $error"));
    currentGuest.guestKey = docRef.id;
  }

  void navigateToHomePage() {
    ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.homePage);
  }

  void navigateToGuestListPage() {
    ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.guestTabPage);
  }

  Future<void> addGuestsFromContactList(List<Contact> selectedContacts) async {
    selectedContacts.forEach((guest) async {
      SelectedGuest tempGuest = SelectedGuest();
      tempGuest.guestPhone = guest.phones.elementAt(0).value ?? "unkown";
      tempGuest.guestName = guest.displayName.toString() ?? "unknown";
      tempGuest.confirmed = false;
      //  tempGuest.guestEmail = guest.emails
      // print(guest.emails.toString());
      //  tempGuest.guestType=guest.company.toString();
      await saveGuestOnFireStore(tempGuest);
    });
  }

  Future<void> addNewCatagory() async {
    if (fbKey2.currentState.saveAndValidate()) {
      print(fbKey2.currentState.value);
      // await saveEventOnFireStore(currentLoggedInUser);
      String newCatagory = fbKey2.currentState.value['relations'];
      await saveCatagoryOnFireStore(newCatagory);
    }
  }

  Future saveCatagoryOnFireStore(String newCatagory) async {
    var firebaseUser = FirebaseAuth.instance.currentUser;
    DocumentReference relations = firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId);
    var list = [newCatagory];
    relations.update({'relations': FieldValue.arrayUnion(list)});
  }

  Future<List> createGuestList() async {
    var firebaseUser = FirebaseAuth.instance.currentUser;
    CollectionReference guests = firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId)
        .collection('Guests');

    QuerySnapshot _myDoc = await guests.get();
    List<DocumentSnapshot> _myDocCount = _myDoc.docs;

    QuerySnapshot _confirmed =
        await guests.where('confirmed', isEqualTo: true).get();
    List<DocumentSnapshot> confirmedCount = _confirmed.docs;

    return [_myDocCount.length, confirmedCount.length];
  }
}


int random(min, max) {
  var rn = new Random();
  return min + rn.nextInt(max - min);
}
