import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:seatmeapp_finalproject/core/entities/user.dart';
import 'package:seatmeapp_finalproject/guestsF/state_managment/guest_list_page_provider.dart';
import 'package:seatmeapp_finalproject/guestsF/widgets/active_guests.dart';
import 'package:seatmeapp_finalproject/core/widgets/not_exist_list.dart';
import 'package:seatmeapp_finalproject/routes/routes.gr.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';

// Guests list main page , all the logic reffers to Guest List Page Provider
class GuestListPage extends StatefulWidget {
  GuestListPage({Key key}) : super(key: key);

  @override
  _GuestListPageState createState() => _GuestListPageState();
}

class _GuestListPageState extends State<GuestListPage> {
  final SeatMeAppEvents currentEvent = getIt();
  final SeatMeAppUser currentUser = getIt();
  final FirebaseFirestore firestoreInstance = FirebaseFirestore.instance;
  Stream stream; //extract all the guests from the data base
  var firebaseUser = FirebaseAuth.instance.currentUser;
  @override
  void initState() {
    super.initState();
    stream = firestoreInstance
        .collection("Users")
        .doc(currentUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId)
        .collection('Guests')
        .snapshots();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
            Text('רשימת המוזמנים שלך', style: TextStyle(color: Colors.black)),
        centerTitle: true,
        backgroundColor: Colors.white,
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_sharp,
            color: Colors.black,
          ),
          onPressed: () {
            ExtendedNavigator.named(RoutesNames.mainNav).pop();
          },
        ),
      ),
      body: Stack(
        children: <Widget>[
          StreamBuilder(
            stream: stream,
            builder: (ctx, dataSnapshot) {
              if (dataSnapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else {
                if (dataSnapshot.error != null) {
                  // ...
                  // Do error handling stuff
                  return Center(
                    child: Text('An error occurred! please try again'),
                  );
                }
                if (dataSnapshot.data.docs.length == 0) {
                  return NotFound(
                      name: "אורחים",
                      onTapCallback: func,
                      icon: Icon(FontAwesomeIcons.calendarPlus,
                          color: Colors.white));
                } else {
                  var guests = dataSnapshot.data.docs;
                  return new ListView(
                      //list view with all guests with their details
                      children: guests.map<Widget>((DocumentSnapshot document) {
                    return new ActiveGuests(
                      document["guestName"],
                      document["guestType"],
                      document["guestPhone"],
                      document["guestEmail"],
                      document.id,
                      int.parse(document['numOfInvites'].toString()),
                    );
                  }).toList());
                }
              }
            },
          ),
          Positioned(
            bottom: 40,
            right: 30,
            child: Container(
              width: 130,
              child: FloatingActionButton(
                  onPressed: () {
                    Provider.of<GuestListPageProvider>(context, listen: false)
                        .navigateToAddGuestPage();
                  },
                  backgroundColor: Colors.deepOrange[400],
                  elevation: 3,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(200),
                  ),
                  child: Text(
                    'להוספת מוזמנים',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                    ),
                  )),
            ),
          ),
        ],
      ),
    );
  }
}

func() {
  ExtendedNavigator.named(RoutesNames.mainNav).push(Routes.guestTabPage);
}
