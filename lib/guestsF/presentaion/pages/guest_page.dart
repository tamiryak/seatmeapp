import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:seatmeapp_finalproject/core/entities/user.dart';
import 'package:seatmeapp_finalproject/guestsF/state_managment/guest_page_provider.dart';

String guest_type;

class GuestPage extends StatelessWidget {
  const GuestPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: Provider.of<GuestPageProvider>(context, listen: false).fbKey,
      initialValue: {},
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
                  child: Container(
            child: Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                TextFieldsEditor(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class TextFieldsEditor extends StatefulWidget {
  TextFieldsEditor({Key key}) : super(key: key);

  @override
  _TextFieldsEditorState createState() => _TextFieldsEditorState();
}

class _TextFieldsEditorState extends State<TextFieldsEditor> {
  DateTime myDate = new DateTime(
      DateTime.now().year, DateTime.now().month, DateTime.now().day);
  @override
  Widget build(BuildContext context) {
    // var now = DateTime.now();
    // var today = new DateTime(now.year, now.month, now.day);
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
    children: [
      Text(
          "אנא מלא פרטי האורח",
          style: TextStyle(
            fontFamily: 'Lobster',
            fontSize: 30,
          ),
      ),
      SizedBox(
          height: 20,
      ),
      GuestTextField(
          name: "guestName",
          lableText: 'שם האורח',
          digitOnly: false,
      ),
      SizedBox(
          height: 20,
      ),
      Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            DropDownList(),
            Text(
              ":קרבה",
              style: TextStyle(fontSize: 20),
            ),
          ],
      ),
      // GuestTextField(
      //   name: "guestType",
      //   lableText: 'קרבה',
      //   digitOnly: false,
      // ),
      SizedBox(
          height: 20,
      ),
      GuestTextField(
            name: "guestPhone",
            lableText: "מספר פלאפון של האורח",
            digitOnly: true,
            maxLength:9
            ),
      SizedBox(
          height: 20,
      ),
      GuestTextField(
          name: "guestEmail",
          lableText: 'אימייל של האורח ',
          digitOnly: false,
          email:true
      ),
      SizedBox(
          height: 20,
      ),
      GuestTextField(
          name: "numOfInvites",
          lableText: 'מספר מוזמנים',
          digitOnly: true,
      ),
      SizedBox(
          height: 20,
      ),
      FloatingActionButton(
          elevation: 5,
          backgroundColor: Colors.deepOrange,
          child: Container(
              width: 100,
              height: 100,
              child: Center(
        child: Text(
                "הוסף אורח",
                textAlign: TextAlign.center,
              ))),
          onPressed: () async {
            await showAlertDialog(context);
          },
      )
    ],
          ),
      );
  }
}

showAlertDialog(BuildContext context) {
  // set up the buttons
  Widget cancelButton = FlatButton(
    child: Text("לא"),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );
  Widget continueButton = FlatButton(
    child: Text("כן"),
    onPressed: () {
      if(Provider.of<GuestPageProvider>(context, listen: false).fbKey.currentState.validate()){
      Provider.of<GuestPageProvider>(context, listen: false)
          .guestComplete(guest_type);
      Navigator.of(context).pop();
      
      Fluttertoast.showToast(
          msg: "האורח הוסף לרשימת המוזמנים",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3);
      }
      else{
        Navigator.of(context).pop();
              Fluttertoast.showToast(
          msg: "הוזן מידע שגוי",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 3);
      }
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("הוספת אורח"),
    content: Text("האם אתה בטוח שאתה רוצה להוסיף את האורח"),
    actions: [
      cancelButton,
      continueButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class GuestTextField extends StatelessWidget {
  final String name;
  final String lableText;
  final bool multiline;
  final bool digitOnly;
  final bool email;
  final int maxLength;
  const GuestTextField(
      {Key key,
      this.multiline = false,
      @required this.name,
      this.lableText,
      @required this.digitOnly, this.maxLength, this.email=false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: FormBuilderTextField(
            inputFormatters: [
              if (digitOnly) FilteringTextInputFormatter.digitsOnly
              // else
              //   FilteringTextInputFormatter.allow(
              //     RegExp("[a-zA-Z0-9]"),
              //   ),
              // LengthLimitingTextInputFormatter(4),
            ],
            validators: [
              FormBuilderValidators.required(),
              //FormBuilderValidators.maxLength(maxLength),
              if(email)FormBuilderValidators.email()
            ],
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: lableText,
            ),
            attribute: name,
          ),
        ),
      ],
    );
  }
}

class DropDownList extends StatefulWidget {
  @override
  _DropDownListState createState() => _DropDownListState();
}

class _DropDownListState extends State<DropDownList> {
  String dropdownValue = 'משפחה';
  final FirebaseFirestore firestoreInstance = FirebaseFirestore.instance;
  List<DropdownMenuItem> relations = [];
  final SeatMeAppUser currentLoggedInUser = getIt();
  final SeatMeAppEvents currentEvent = getIt();
  var firebaseUser = FirebaseAuth.instance.currentUser;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        initialData: ['משפחה'],
        stream: firestoreInstance
            .collection("Users")
            .doc(firebaseUser.uid)
            .collection('Events')
            .doc(currentEvent.eventId)
            .snapshots(),
        builder: (context, snapshot) {
          var doc = snapshot.data;
          relations.clear();
          var doc2 = doc['relations'];
          // doc2.sort((a, b) {
          //   return a.toLowerCase().compareTo(b.toLowerCase());
          // });
          if (!snapshot.hasData) {
            Container(child: CircularProgressIndicator());
          } else {
            for (int i = 0; i < doc2.length; i++) {
              String snap = doc2[i];
              if (snap != 'הוספת קטגוריות')
                relations.add(DropdownMenuItem(
                    value: snap, child: Center(child: Text(snap))));
            }
            String snap = 'הוספת קטגוריות';
            relations.add(DropdownMenuItem(
                value: snap,
                child: TextButton(
                  onPressed: () {
                    Dialog addCatagoryDialog = Dialog(
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(12.0)), //this right here
                      child: Container(
                        height: 300.0,
                        width: 300.0,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: 250,
                              child: FormBuilder(
                                key: Provider.of<GuestPageProvider>(context,
                                        listen: false)
                                    .fbKey2,
                                child: GuestTextField(
                                  name: "relations",
                                  lableText: 'קרבה',
                                  digitOnly: false,
                                ),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(top: 50.0)),
                            FlatButton(
                                onPressed: () async {
                                  Provider.of<GuestPageProvider>(context,
                                          listen: false)
                                      .addNewCatagory();

                                  Navigator.of(context).pop();
                                  Navigator.of(context).pop();

                                  //provider
                                },
                                child: Text(
                                  'הוסף קטגוריה!',
                                  style: TextStyle(
                                      color: Colors.blue, fontSize: 18.0),
                                ))
                          ],
                        ),
                      ),
                    );
                    showDialog(
                        context: context,
                        builder: (BuildContext context) => addCatagoryDialog);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(snap),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 2),
                        child: Icon(Icons.add_box_rounded, color: Colors.green),
                      ),
                    ],
                  ),
                )));
          }

          return DropdownButton(
            onChanged: (newValue) {
              setState(() {
                dropdownValue = newValue.toString();
                guest_type = dropdownValue;
              });
            },
            value: dropdownValue,
            items: relations,
            hint: Text('קרבה'),
          );
        });
  }
}
