import 'package:flutter/material.dart';
import 'package:seatmeapp_finalproject/guestsF/presentaion/pages/contacts_phone_page.dart';
import 'package:seatmeapp_finalproject/guestsF/presentaion/pages/guest_page.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';
import 'package:auto_route/auto_route.dart';

class GuestTabPage extends StatelessWidget {
  const GuestTabPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_sharp,
              color: Colors.black,
            ),
            onPressed: () {
              ExtendedNavigator.named(RoutesNames.mainNav).pop();
            },
          ),
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            "SeatMeApp",
            style: TextStyle(
                fontFamily: 'Lobster', fontSize: 25, color: Colors.black),
          ),

          bottom: TabBar(
            indicatorColor: Colors.grey,
            labelColor: Colors.black,
            unselectedLabelColor: Colors.grey,
            tabs: [
              Tab(
                icon: Icon(
                  Icons.add,
                  color: Colors.black,
                ),
                text: 'הוספת אורח',
              ),
              Tab(
                icon: Icon(
                  Icons.phone,
                  color: Colors.black,
                ),
                text: 'הוסף מרשימת אנשי קשר',
              ),
            ],
          ),
          // title: Text('Tabs Demo'),
        ),
        body: TabBarView(
          children: [
            GuestPage(),
            ContactsPage(),
          ],
        ),
      ),
    );
  }
}
