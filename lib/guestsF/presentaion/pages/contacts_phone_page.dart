import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

import 'package:seatmeapp_finalproject/guestsF/state_managment/guest_page_provider.dart';

class ContactsPage extends StatefulWidget {
  ContactsPage({Key key}) : super(key: key);

  @override
  _ContactsPageState createState() => _ContactsPageState();
}

class _ContactsPageState extends State<ContactsPage> {
  List<Contact> contacts = [];
  List<Contact> selectedContacts = [];
  List<Contact> filteredContacts = [];
  TextEditingController searchController = new TextEditingController();
  bool flag_no_phone_num = false;

  @override
  void initState() {
    super.initState();
    getAllContacts();
    searchController.addListener(() {
      if (searchController.text.startsWith(RegExp('[0-9]')))
        filterContactsByNumber();
      else
        filterContacts();
    });
  }

  String flattenPhoneNumber(String phoneStr) {
    phoneStr.replaceAll(RegExp(r'^(\+)|\D'), '');
  }

  getAllContacts() async {
    List<Contact> _contacts = (await ContactsService.getContacts()).toList();
    setState(() {
      contacts = _contacts;
    });
  }

  filterContacts() {
    List<Contact> _contacts = [];
    _contacts.addAll(contacts);
    if (searchController.text.isNotEmpty) {
      _contacts.retainWhere((contact) {
        String searchTerm = searchController.text.toLowerCase();
        String contactName = contact.displayName.toLowerCase();
        bool nameMatches = contactName.contains(searchTerm);
        if (nameMatches) return true;
        var phone = contact.phones.firstWhere((phn) {
          return phn.value.contains(searchTerm);
        }, orElse: () => null);

        return phone != null;
      });
      setState(() {
        filteredContacts = _contacts;
      });
    } else {
      setState(() {
        filteredContacts = contacts;
      });
    }
  }

  filterContactsByNumber() {
    List<Contact> _contacts = [];
    _contacts.addAll(contacts);
    if (searchController.text.isNotEmpty) {
      _contacts.retainWhere((contact) {
        String searchTerm = searchController.text.toLowerCase();
        String searchTermFlatten = flattenPhoneNumber(searchTerm);
        String contactName = contact.displayName.toLowerCase();
        bool nameMatches = contactName.contains(searchTerm);
        if (nameMatches) return true;
        if (searchTermFlatten.isEmpty) return false;
        var phone = contact.phones.firstWhere((phn) {
          String phnFlattend = flattenPhoneNumber(phn.value);
          return phnFlattend.contains(searchTerm);
        }, orElse: () => null);
        return phone != null;
      });
      setState(() {
        filteredContacts = _contacts;
      });
    }
  }

  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("לא"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = FlatButton(
      child: Text("כן"),
      onPressed: () {
        if (!flag_no_phone_num) {
          Provider.of<GuestPageProvider>(context, listen: false)
              .addGuestsFromContactList(selectedContacts);
        }
        Navigator.of(context).pop();
        Fluttertoast.showToast(
            msg: "האורחים הוספו לרשימת המוזמנים",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 3);
      },
    );

    AlertDialog alert;
    // set up the AlertDialog
    if (selectedContacts.isEmpty) {
      alert = AlertDialog(
        title: Text("הוספת אורחים"),
        content: Text("!לא סימנת אף אורח"),
      );
    } else {
      alert = AlertDialog(
        title: Text("הוספת אורחים"),
        content: Text("?האם אתה בטוח שאתה רוצה להוסיף את האורחים"),
        actions: [
          cancelButton,
          continueButton,
        ],
      );
    }
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    void _onContactSelected(bool selected, id) {
      if (selected == true) {
        setState(() {
          selectedContacts.add(id);
        });
      } else {
        setState(() {
          selectedContacts.remove(id);
        });
      }
    }

    bool isSearching = searchController.text.isNotEmpty;
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 40,
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.8,
            child: TextField(
              controller: searchController,
              decoration: InputDecoration(
                labelText: 'חיפוש',
                border: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Theme.of(context).primaryColor)),
                prefixIcon: Icon(
                  Icons.search,
                  color: Theme.of(context).primaryColor,
                ),
              ),
            ),
          ),
          Expanded(
              child: ListView.builder(
            shrinkWrap: true,
            itemCount: isSearching ? filteredContacts.length : contacts.length,
            itemBuilder: (context, index) {
              Contact contact =
                  isSearching ? filteredContacts[index] : contacts[index];
              return CheckboxListTile(
                title: Text(contact.displayName),

                subtitle: Text(
                  (contact.phones.isNotEmpty)
                      ? contact.phones.elementAt(0).value
                      : "לא הוזן מספר במכשיר",
                ),

                onChanged: (bool value) {
                  if (contact.phones.isNotEmpty) {
                    _onContactSelected(value, contacts[index]);
                  }
                },
                value: selectedContacts.contains(contacts[index]),

                // leading: (contact.avatar != null && contact.avatar.length > 0)
                //     ? CircleAvatar(
                //         backgroundImage: MemoryImage(contact.avatar),
                //       )
                //     : CircleAvatar(
                //         child: Text(contact.initials()),
                //       )
              );
            },
          )),
          Container(
            width: MediaQuery.of(context).size.width * 0.35,
            child: FloatingActionButton(
                onPressed: () async {
                  await showAlertDialog(context);
                },
                backgroundColor: Colors.deepOrange[400],
                elevation: 3,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(200),
                ),
                child: Text(
                  'הוסף מוזמנים',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                  ),
                )),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.05,
          ),
        ],
      ),
    );
  }
}
