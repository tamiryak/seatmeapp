import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:seatmeapp_finalproject/core/entities/selected_guest.dart';
import 'package:seatmeapp_finalproject/guestsF/presentaion/pages/update_guest_page.dart';
import 'package:seatmeapp_finalproject/guestsF/state_managment/guest_list_page_provider.dart';

class ActiveGuests extends StatefulWidget {
  final String guestName;
  final String guestType;
  final String guestPhone;
  final String guestEmail;
  final String guestKey;
  final int numOfInvites;

  const ActiveGuests(this.guestName, this.guestType, this.guestPhone,
      this.guestEmail, this.guestKey, this.numOfInvites);

  @override
  _ActiveGuestsState createState() => _ActiveGuestsState();
}

class _ActiveGuestsState extends State<ActiveGuests> {
  bool phoneShow = false;
  bool emailShow = false;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Dismissible(
          key: ValueKey(widget.guestKey),
          background: Container(
            color: Theme.of(context).errorColor,
            child: Icon(
              Icons.delete,
              color: Colors.white,
              size: 40,
            ),
            alignment: Alignment.centerRight,
            padding: EdgeInsets.only(right: 20),
            margin: EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 4,
            ),
          ),
          direction: DismissDirection.endToStart,
          confirmDismiss: (direction) {
            return showDialog(
              context: context,
              builder: (ctx) => AlertDialog(
                title: Text('אתה בטוח?'),
                content: Text('האם אתה בטוח שאתה רוצה למחוק את האורח?'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('לא'),
                    onPressed: () {
                      Navigator.of(ctx).pop(false);
                    },
                  ),
                  FlatButton(
                    child: Text('כן'),
                    onPressed: () {
                      Navigator.of(ctx).pop(true);
                    },
                  ),
                ],
              ),
            );
          },
          onDismissed: (direction) {
            Provider.of<GuestListPageProvider>(context, listen: false)
                .removeItem(widget.guestKey);
          },
          child: Card(
            margin: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
            child: Padding(
              padding: EdgeInsets.all(8),
              child: ListTile(
                // leading: CircleAvatar(
                //   child: FittedBox(
                //       // child: Text(('$date')),
                //       ),
                // ),
                title: Container(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(' ההזמנה של ${widget.guestName}'),
                    Column(
                      children: [
                        Row(children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                phoneShow = true;
                                emailShow = false;
                              });
                            },
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(6.0),
                              child: Container(
                                child: Center(
                                  child: Icon(Icons.phone),
                                ),
                                height: 30,
                                width: 30,
                                color: Colors.blue[200],
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                emailShow = true;
                                phoneShow = false;
                              });
                            },
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(6.0),
                              child: Container(
                                child: Center(child: Icon(Icons.email)),
                                height: 30,
                                width: 30,
                                color: Colors.blue[200],
                              ),
                            ),
                          )
                        ]),
                        SizedBox(
                          height: 10,
                        ),
                        Column(children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(6.0),
                            child: Container(
                              child: Center(
                                  child: Text(
                                "${this.widget.numOfInvites} :מוזמנים",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 13),
                              )),
                              height: 20,
                              width: 65,
                              color: Colors.green[300],
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          ClipRRect(
                            borderRadius: BorderRadius.circular(6.0),
                            child: Container(
                              child: Center(
                                  child: Text(
                                "${this.widget.guestType}",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 13),
                              )),
                              height: 20,
                              width: 65,
                              color: Colors.redAccent[200],
                            ),
                          )
                        ]),
                      ],
                    )
                  ],
                )),
                onTap: () {
                  print("update guest method");
                  SelectedGuest selectedGuest = SelectedGuest();
                  selectedGuest.guestEmail = widget.guestEmail;
                  selectedGuest.guestName = widget.guestName;
                  selectedGuest.guestPhone = widget.guestPhone;
                  selectedGuest.guestType = widget.guestType;
                  selectedGuest.guestKey = widget.guestKey;
                  selectedGuest.numOfInvites = widget.numOfInvites;
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          UpdateGuestPage(selectedGuest: selectedGuest),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
        phoneShow
            ? FutureBuilder(
                future: Future.delayed(Duration(seconds: 2)),
                builder: (c, s) => s.connectionState != ConnectionState.done
                    ? Positioned(
                        right: 30,
                        bottom: 45,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Colors.black38,
                            ),
                            borderRadius: BorderRadius.all(
                              Radius.circular(35),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(widget.guestPhone),
                          ),
                        ),
                      )
                    : Container(),
              )
            : Container(),
        emailShow
            ? FutureBuilder(
                future: Future.delayed(Duration(seconds: 2)),
                builder: (c, s) => s.connectionState != ConnectionState.done
                    ? Positioned(
                        right: 30,
                        bottom: 45,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Colors.black38,
                            ),
                            borderRadius: BorderRadius.all(
                              Radius.circular(35),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(widget.guestEmail),
                          ),
                        ),
                      )
                    : Container(),
              )
            : Container()
      ],
    );
  }
}
