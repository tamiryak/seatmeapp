import 'dart:ui';

class CanvasObject<T> { //canvas object represent the table on the hall demonstration
  final double dx;
  final double dy;
  final double width;
  final double height;
  final T child;
  final String tableKey;

  CanvasObject({
    this.tableKey='',
    this.dx = 0,
    this.dy = 0,
    this.width = 100,
    this.height = 100,
    this.child,
  });

  CanvasObject<T> copyWith({
    double dx,
    double dy,
    double width,
    double height,
    String tableKey,
    T child,
  }) {
    return CanvasObject<T>(
      dx: dx ?? this.dx,
      dy: dy ?? this.dy,
      width: width ?? this.width,
      height: height ?? this.height,
      child: child ?? this.child,
      tableKey: tableKey??this.tableKey
    );
  }
//getters
  Size get size => Size(width, height);
  double get(String val) {
    if(val=='dx')return dx;
    if(val=='dy')return dy;
  }
  Offset get offset => Offset(dx, dy);
  Rect get rect => offset & size;
}
