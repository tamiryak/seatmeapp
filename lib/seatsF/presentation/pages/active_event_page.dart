import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:seatmeapp_finalproject/core/widgets/not_exist_list.dart';
import 'package:seatmeapp_finalproject/routes/routes.gr.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';
import 'package:seatmeapp_finalproject/seatsF/presentation/state_managment/active_events_provider.dart';
import 'package:seatmeapp_finalproject/seatsF/presentation/widgets/active_events.dart';

class EventSelectionPage extends StatelessWidget {
  const EventSelectionPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('האירועים שלך', style: TextStyle(color: Colors.black)),
        centerTitle: true,
        backgroundColor: Colors.white,
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_sharp,
            color: Colors.black,
          ),
          onPressed: () {
            Provider.of<EventListProvider>(context, listen: false)
                .navigateToHomePage(); //back button
          },
        ),
      ),
      body: FutureBuilder( //create the future builder to fetch all the active events
        future: Provider.of<EventListProvider>(context, listen: false)
            .createEventList(),
        builder: (ctx, dataSnapshot) {
          if (dataSnapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator()); //if still fetching
          } else {
            if (dataSnapshot.error != null) {
              // ...
              // Do error handling stuff
              return Center(
                child: Text('An error occurred! please try again'),
              );
            } else {
              return Consumer<EventListProvider>( //create the event list on a list view builder
                  builder: (ctx, _items, child) => (_items.items.length != 0)
                      ? ListView.builder(
                          itemCount: _items.items.length,
                          itemBuilder: (ctx, i) => ActiveEvents(
                              id: _items.items[i].eventId,
                              eventId: _items.items[i].eventId,
                              date: _items.items[i].eventDate,
                              name: _items.items[i].eventName),
                        )
                      : NotFound( //if not found any event - showing to the user add event button page
                          name: "אירוע",
                          onTapCallback: func,
                          icon:Icon(FontAwesomeIcons.calendarPlus,color:Colors.white)
                        ));
            }
          }
        },
      ),
    );
  }

  func() {
    ExtendedNavigator.named(RoutesNames.mainNav)
        .push(Routes.openEventPage);
    // print("im working");
  }
}
