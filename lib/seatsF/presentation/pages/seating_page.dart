import 'package:auto_route/auto_route.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/routes/routes.gr.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';
import 'package:seatmeapp_finalproject/seatsF/core/controllers/canvas.dart';
import 'package:seatmeapp_finalproject/core/selected_table.dart';
import 'package:seatmeapp_finalproject/seatsF/presentation/state_managment/seating_page_provider.dart';

class SeatingF extends StatefulWidget {
  const SeatingF({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<SeatingF> {
  final _controller = CanvasController(); //controller for the tables objects
  SelectedTable selectedTable = getIt();
  bool flag1 = false;
  bool flag2 = false;
  Offset p1;
  Offset p2;
  Offset p3;
  Offset p4;

  @override
  void initState() {
    super.initState();
    Provider.of<SeatingPageProvider>(context, listen: false)
        .createHall(_controller); //creating the hall with the provider
    _controller.init(); //initalize the variables of the controller 
    //_dummyData();
  }

  void tables_get_dx() {
    _controller.objects.forEach((element) {
      print('${element.dx}');
    });
  }

  @override
  void dispose() {
    _controller.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<CanvasController>( //stream builder for the contoller - if succeed to get objects to show
        stream: _controller.stream,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Scaffold(
              appBar: AppBar(),
              body: Center(child: CircularProgressIndicator()),
            );
          }
          final instance = snapshot.data;
          //instance.objects.sort((a,b)=>a.dy.compareTo(b.dy));
          return Scaffold(
            appBar: AppBar( //app bar - zoom/multiselect/back button
              backgroundColor: Colors.black54,
              actions: [
                FocusScope(
                  canRequestFocus: false,
                  child: IconButton(
                      tooltip: 'Selection',
                      icon: Icon(Icons.accessibility),
                      color: instance.shiftPressed
                          ? Theme.of(context).accentColor
                          : null,
                      onPressed: () {
                        navigateToHomePage();
                      }),
                ),
                FocusScope(
                  canRequestFocus: false,
                  child: IconButton(
                    tooltip: 'Meta Key',
                    color: instance.metaPressed
                        ? Theme.of(context).accentColor
                        : null,
                    icon: Icon(Icons.category),
                    onPressed: _controller.metaSelect, //on pressed - controller recieve signal from the user
                  ),
                ),
                FocusScope(
                  canRequestFocus: false,
                  child: IconButton(
                    tooltip: 'Zoom In',
                    icon: Icon(Icons.zoom_in),
                    onPressed: _controller.zoomIn,
                  ),
                ),
                FocusScope(
                  canRequestFocus: false,
                  child: IconButton(
                    tooltip: 'Zoom Out',
                    icon: Icon(Icons.zoom_out),
                    onPressed: _controller.zoomOut,
                  ),
                ),
                FocusScope(
                  canRequestFocus: false,
                  child: IconButton(
                    tooltip: 'Reset the Scale and Offset',
                    icon: Icon(Icons.restore),
                    onPressed: _controller.reset,
                  ),
                ),
              ],
            ),
            body: Listener( //listener to get the zoom/ movment of the objects
              behavior: HitTestBehavior.opaque,
              onPointerSignal: (details) {
                if (details is PointerScrollEvent) {
                  GestureBinding.instance.pointerSignalResolver
                      .register(details, (event) {
                    if (event is PointerScrollEvent) {
                      if (_controller.shiftPressed) { //if zoom change
                        double zoomDelta = (-event.scrollDelta.dy / 300);
                        _controller.scale = _controller.scale + zoomDelta;
                      } else {
                        _controller.offset =
                            _controller.offset - event.scrollDelta; //if the offset of the page change
                      }
                    }
                  });
                }
              },
              onPointerMove: (details) { //on every touch on object - do func
                _controller.updateTouch(details.pointer, details.localPosition,
                    details.position, context);

                flag1 = false; // indicate horizontal flag
                flag2 = false; // indicate Vertical flag
                _controller.objects.forEach((element) {
                  double dxAbs = selectedTable.dx -
                      element.dx; //checks differnce betwen 2 tables
                  double dyAbs = selectedTable.dy - element.dy;
                  if (dxAbs > -0.3 &&
                      dxAbs <
                          0.3 && //if 2 tables have same dx and they arent sames tables =>glag1=True
                      selectedTable.tableKey != element.tableKey) {
                    flag1 = true;
                    p1 = Offset(selectedTable.dx + 50,
                        selectedTable.dy + 50); // offset to print the DX line
                    p2 = Offset(element.dx + 50,
                        element.dy + 50); // offset to print the DX line
                  }
                  if (dyAbs >
                          -0.3 && //if 2 tables have same DY and they arent sames tables =>glag2=True
                      dyAbs < 0.3 &&
                      selectedTable.tableKey != element.tableKey) {
                    flag2 = true;
                    p3 = Offset(selectedTable.dx + 50, selectedTable.dy + 50);
                    p4 = Offset(element.dx + 50, element.dy + 50);
                  }
                });
                if (flag1) {}
              },
              onPointerDown: (details) { //if touch on object
                _controller.addTouch(
                  details.pointer,
                  details.localPosition,
                  details.position,
                );
              },
              onPointerUp: (details) { //if touch on screen - remove touch
                _controller.removeTouch(details.pointer);
                flag1 = false;
                flag2 = false;
              },
              onPointerCancel: (details) {
                _controller.removeTouch(details.pointer);
              },
              child: RawKeyboardListener( //showing the movement on the screen listener (zoom)
                autofocus: true,
                focusNode: _controller.focusNode,
                onKey: (key) => _controller.rawKeyEvent(context, key),
                child: SizedBox.expand(
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                                      child: SingleChildScrollView(
                                        child: Stack(
                        children: [
                          SizedBox.fromSize( //background of the hall
                            size:Size(MediaQuery.of(context).size.width,
                                    MediaQuery.of(context).size.height)*_controller.scale*1.8,
                            child: Image.asset(
                              'lib/core/assets/venue.jpg',
                              fit: BoxFit.fill,
                            ),
                          ),

                          (flag1) // Print the DX line on the screen
                              ? CustomPaint(
                                  //  <-- CustomPaint widget
                                  size: Size(MediaQuery.of(context).size.width,
                                      MediaQuery.of(context).size.height),
                                  painter: MyPainter(p1, p2),
                                )
                              : Container(),
                          (flag2)
                              ? CustomPaint(
                                  //  <-- CustomPaint widget
                                  size: Size(MediaQuery.of(context).size.width,
                                      MediaQuery.of(context).size.height),
                                  painter: MyPainter(p3, p4),
                                )
                              : Container(),
                          for (var i = instance.objects.length - 1; i > -1; i--) //showing the objects - tables on the screen
                            Positioned.fromRect(
                              rect: instance.objects[i].rect.adjusted( //if changing the object dx'dy
                                _controller.offset,
                                _controller.scale,
                              ),
                              child: Container( //on pressed - showing grey selection stroke
                                decoration: BoxDecoration(
                                    border: Border.all(
                                  color: instance.isObjectSelected(i)
                                      ? Colors.grey
                                      : Colors.transparent,
                                )),
                                child: GestureDetector( //changing the size of objects by the zoom
                                  onTapDown: (_) => _controller.selectObject(i),
                                  child: FittedBox(
                                    fit: BoxFit.fill,
                                    child: SizedBox.fromSize(
                                      size: instance.objects[i].size,
                                      child: instance.objects[i].child,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          if (instance?.marquee != null)
                            Positioned.fromRect(
                              rect: instance.marquee.rect
                                  .adjusted(instance.offset, instance.scale),
                              child: Container(
                                color: Colors.blueAccent.withOpacity(0.3),
                              ),
                            ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        });
  }
}

extension RectUtils on Rect {
  Rect adjusted(Offset offset, double scale) {
    final left = (this.left + offset.dx) * scale;
    final top = (this.top + offset.dy) * scale;
    final width = this.width * scale;
    final height = this.height * scale;
    return Rect.fromLTWH(left, top, width, height);
  }
}

void navigateToHomePage() {
  ExtendedNavigator.named(RoutesNames.mainNav).pop(Routes.homePage);
}

class MyPainter extends CustomPainter {
  //         <-- CustomPainter class
  final Offset p1;
  final Offset p2;
  MyPainter(this.p1, this.p2);

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Colors.deepOrange[300]
      ..strokeWidth = 2;
    canvas.drawLine(p1, p2, paint);
    //sleep(Duration(milliseconds: 500));
  }

  @override
  bool shouldRepaint(CustomPainter old) {
    return false;
  }
}
