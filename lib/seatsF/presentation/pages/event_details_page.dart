import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:seatmeapp_finalproject/core/entities/user.dart';
import 'package:seatmeapp_finalproject/routes/routes.gr.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';
import 'package:seatmeapp_finalproject/loginF/core/shared_preferences_names.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EventDetailsPage extends StatefulWidget {
  const EventDetailsPage({Key key}) : super(key: key);

  @override
  _EventDetailsPageState createState() => _EventDetailsPageState();
}

class _EventDetailsPageState extends State<EventDetailsPage> {
  final SeatMeAppEvents currentEvent = getIt();
  final SeatMeAppUser currentLoggedInUser = getIt();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 10,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(70),
            ),
          ),
          title: Text( //showing the title of the event 
            "${currentEvent.eventName} :עמוד אירוע ",
            style: TextStyle(fontFamily: 'Lobster', color: Colors.white),
          ),
          centerTitle: true,
          backgroundColor: Colors.deepOrange,
        ),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  height: 20,
                ),
                Text("${currentEvent.eventName}",
                    style: TextStyle(
                        color: Colors.black87,
                        fontSize: MediaQuery.of(context).size.height * 0.06,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Lobster')),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: 200,
                  child: FloatingActionButton(
                      onPressed: () {
                        navigateToSeatingF();
                      },
                      backgroundColor: Colors.deepOrange,
                      elevation: 3,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(200),
                      ),
                      child: Text(
                        'תצוגת אולם',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                        ),
                      )),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: 200,
                  child: FloatingActionButton(
                      onPressed: () {
                        navigateToGuestListPage();
                      },
                      backgroundColor: Colors.deepOrange,
                      elevation: 3,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(200),
                      ),
                      child: Text(
                        'רשימת מוזמנים',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                        ),
                      )),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: 200,
                  child: FloatingActionButton(
                      onPressed: () {
                        navigateToRsvpPage();
                      },
                      backgroundColor: Colors.deepOrange,
                      elevation: 3,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(200),
                      ),
                      child: Center(
                        child: Text(
                          'מעבר למערכת אישורי הגעה ',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                          ),
                        ),
                      )),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: 200,
                  child: FloatingActionButton(
                      onPressed: () {
                        navigateToHomePage();
                      },
                      backgroundColor: Colors.deepOrange,
                      elevation: 3,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(200),
                      ),
                      child: Text(
                        'חזרה לעמוד הראשי',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                        ),
                      )),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

void navigateToSeatingF() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setInt(HAVE_TABLES, 2);
  ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.seatingF);
}

void navigateToGuestPage() async {
  ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.guestPage);
}

void navigateToGuestListPage() async {
  ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.guestListPage);
}

void navigateToRsvpPage() async {
  ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.confirmationTabPage);
}

void navigateToHomePage() async {
  ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.homePage);
}
  