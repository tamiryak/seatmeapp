import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:seatmeapp_finalproject/core/entities/selected_guest.dart';
import 'package:seatmeapp_finalproject/core/widgets/not_exist_list.dart';
import 'package:seatmeapp_finalproject/routes/routes.gr.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';
import 'package:seatmeapp_finalproject/seatsF/presentation/widgets/table_widget.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

Stream stream;

class TableSeatPage extends StatefulWidget {
  final String tableKey;
  final bool isPlus;
  TableSeatPage({Key key, this.tableKey, this.isPlus}) : super(key: key);

  @override
  _TableSeatPageState createState() => _TableSeatPageState();
}


int selectedIndex = -1;//user not selected yes
SelectedGuest selectedGuest = getIt();

class _TableSeatPageState extends State<TableSeatPage> {
  final SeatMeAppEvents currentEvent = getIt();
  var firebaseUser = FirebaseAuth.instance.currentUser;
  final FirebaseFirestore firestoreInstance = FirebaseFirestore.instance;
  @override
  void initState() {
    super.initState();
    resetSelectedGuest();
    stream = firestoreInstance //stream of all guests
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId)
        .collection('Guests')
        .snapshots();
  }

  @override
  Widget build(BuildContext context) {
    //var tableKey = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_sharp,
            color: Colors.black,
          ),
          onPressed: () {
            ExtendedNavigator.named(RoutesNames.mainNav).pop();
          },
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          "SeatMeApp",
          style: TextStyle(
              fontFamily: 'Lobster', fontSize: 18, color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
              child: Column(children: <Widget>[
          Row(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  height: MediaQuery.of(context).size.height * 0.45,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey,
                  child: TableWidget(300, 250,
                      widget.tableKey ?? currentEvent.tableSelected, false)), //showing table which we select
            ],
          ),
          const Divider(
            height: 0,
            thickness: 2,
            indent: 0,
            color: Colors.black38,
            endIndent: 0,
          ),
          SingleChildScrollView(
            child: StreamBuilder( //stream showing a list of all guests we added
              stream: stream,
              builder: (ctx, dataSnapshot) {
                var data = dataSnapshot.data.docs;
                if (dataSnapshot.connectionState == ConnectionState.waiting) {
                  return Center(child: CircularProgressIndicator());
                } else {
                  if (dataSnapshot.error != null) {
                    // ...
                    // Do error handling stuff
                    return Center(
                      child: Text('An error occurred! please try again'),
                    );
                  } else if (!dataSnapshot.hasData) {
                    return NotFound(//if no guest was added
                        name: "אורח",
                        onTapCallback: func,
                        icon:
                            Icon(FontAwesomeIcons.userPlus, color: Colors.white));
                  } else {
                    return ListView.builder( //create the list of guests
                        primary: false,
                        shrinkWrap: true,
                        itemCount: data.length,
                        itemBuilder: (_, i) {
                          return GuestListTile(
                              data: data, index: i); // reverse bool value
                        });
                  }
                }
              },
            ),
          )
        ]),
      ),
    );
  }
}

class GuestListTile extends StatefulWidget {
  GuestListTile({
    Key key,
    @required this.data,
    this.index,
  }) : super(key: key);

  var data;
  int index;

  @override
  _GuestListTileState createState() => _GuestListTileState();
}

class _GuestListTileState extends State<GuestListTile> {
  bool selected=false;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 2),
      color: selected
          ? Colors.blue[200]
          : null, // if current item is selected show blue color(
      child: ListTile(
          title: Text("${widget.data[widget.index]["guestName"]}"),
          onTap: () {
            if (selectedIndex == -1) {
              //if not select a guest yet
              setState(() {
                copySelectedGuest(widget.data[widget.index]); //copy the selected guest to save his info
                selectedIndex = widget.index; //showing the tile selected
                selected = true;
                //print(_selected[widget.index]);
              });
            } else {
              if (selectedIndex == widget.index) {
                setState(() {
                  resetSelectedGuest();
                  selectedIndex = -1;
                  selected = false;
                  //print(_selected[widget.index]);
                });
              }
            }
          }),
    );
  }
}

func() {
  ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.guestTabPage);
}

void resetSelectedGuest() {
  selectedGuest.confirmed = false;
  selectedGuest.guestEmail = "";
  selectedGuest.guestKey = "";
  selectedGuest.guestName = "";
  selectedGuest.guestPhone = "";
  selectedGuest.guestType = "";
  selectedGuest.numOfInvites = 0;
}

void copySelectedGuest(var guest) {
  if (guest != null) {
    selectedGuest.confirmed = guest["confirmed"];
    selectedGuest.guestEmail = guest["guestEmail"];
    selectedGuest.guestKey = guest.id;
    selectedGuest.guestName = guest["guestName"];
    selectedGuest.guestPhone = guest["guestPhone"];
    selectedGuest.guestType = guest["guestType"];
    selectedGuest.numOfInvites = int.parse(guest["numOfInvites"].toString());
  }
}
