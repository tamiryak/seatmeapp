import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:seatmeapp_finalproject/core/entities/user.dart';
import 'package:seatmeapp_finalproject/core/selected_table.dart';
import 'package:seatmeapp_finalproject/loginF/core/shared_preferences_names.dart';
import 'package:seatmeapp_finalproject/seatsF/core/classes/canvas_object.dart';
import 'package:seatmeapp_finalproject/seatsF/core/controllers/canvas.dart';
import 'package:seatmeapp_finalproject/seatsF/presentation/widgets/table_widget.dart';
import 'dart:math';

import 'package:shared_preferences/shared_preferences.dart';

class SeatingPageProvider with ChangeNotifier {
  final SeatMeAppUser currentLoggedInUser;
  final SeatMeAppEvents currentEvent;
  final SelectedTable selectedObject;
  final FirebaseFirestore firestoreInstance;
  final controller = CanvasController();
  var firebaseUser = FirebaseAuth.instance.currentUser;

  SeatingPageProvider()
      : currentLoggedInUser = getIt(),
        currentEvent = getIt(),
        selectedObject = getIt(),
        firestoreInstance = FirebaseFirestore.instance;

//create the entire hall info and save it on firebase
  Future<void> createHall(CanvasController controller) async {
    CollectionReference events = firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId)
        .collection('Tables');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if ((prefs.getInt('HAVE_TABLES') == 1)) {
      //generate key of firebase and this key will be saved as event key for loading the objects
      saveTablesOnFirebase(events);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setInt(HAVE_TABLES, 2);
    }
    loadTables(controller, events);
  }

  Future<void> saveTablesOnFirebase(CollectionReference events) async {
    //create new objects and save them to firebase.
    int numberOfTables =
        (currentEvent.numberOfGuests / currentEvent.numOfChairsOnTable)
                .round() +
            1;
    int sqNum = sqrt(numberOfTables).toInt();
    int j = 0;
    for (int i = 1; i <= numberOfTables; i++) {
      int k = i % sqNum;
      double height = 10 * (currentEvent.numOfChairsOnTable.toDouble());
      if (height < 100) height = 100;
      events.add({
        'dx': 100 + k.toDouble() * (height + 30), //spaces between tables
        'dy': 150 + j.toDouble() * 130,
        'width': 100,
        'height': height,
      });

      if (i % sqNum == 0) {
        j++;
      }
    }
  }
//load the data from firebase to place on the controller
  void loadTables(CanvasController controller, CollectionReference events) {
    events.get().then((value) => {
          value.docs.forEach((element) {
            double w = element.get('width').toDouble();
            double h = element.get('height').toDouble();
            controller.addObject(
              CanvasObject(
                dx: element.get('dx').toDouble(),
                dy: element.get('dy').toDouble(),
                width: w,
                height: h,
                tableKey: element.id,
                child: TableWidget(
                    w, h, element.id, true), //Container(color: Colors.brown),
              ),
            );
          })
        });
  }
//update position when changed on screen to firebase
  void updatePosition() {
    //update position for each table at db
    DocumentReference table = firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId)
        .collection('Tables')
        .doc(selectedObject.tableKey);

    table.update(<String, dynamic>{
      'dx': selectedObject.dx,
      'dy': selectedObject.dy,
      'height': selectedObject.height,
      'width': selectedObject.width
    });

    print(selectedObject.tableKey);
  }
}
