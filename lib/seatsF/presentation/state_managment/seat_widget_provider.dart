import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:seatmeapp_finalproject/core/entities/selected_guest.dart';
import 'package:seatmeapp_finalproject/core/entities/user.dart';

class SeatWidgetProvider with ChangeNotifier {
  final SeatMeAppUser currentLoggedInUser;
  final SeatMeAppEvents currentEvent;
  final SelectedGuest currentGuest;
  final firestoreInstance = FirebaseFirestore.instance;

   SeatWidgetProvider()
      : currentLoggedInUser = getIt(),
        currentGuest = getIt(),
        currentEvent = getIt();

  //save the guest we want on firebase
  saveGuestOnTableFireStore(SelectedGuest guestToSave,String seat,String tableKey)  {
    var firebaseUser = FirebaseAuth.instance.currentUser;
    DocumentReference guest = firestoreInstance 
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId)
        .collection('Guests').doc(guestToSave.guestKey);

    guest
        .update({
          'seat': seat ?? "",
          'tableKey' : tableKey?? ""

        })
        .then((value) => value)
        .catchError((error) => print("Failed to add user to table: $error"));
  }

}
