import 'dart:math';

import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:seatmeapp_finalproject/routes/routes.gr.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';
import 'package:seatmeapp_finalproject/loginF/core/shared_preferences_names.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EventListProvider with ChangeNotifier {
  final FirebaseFirestore firestoreInstance;
  var firebaseUser = FirebaseAuth.instance.currentUser;
  final SeatMeAppEvents currentEvent;

  EventListProvider()
      : firestoreInstance = FirebaseFirestore.instance,
        currentEvent = getIt();

//list of all events - at start its empty - firebase fetch to it
  List<SeatMeAppEvents> _items = [];

  List<SeatMeAppEvents> get items {
    return [..._items];
  }

  void addItem(SeatMeAppEvents event) {
    _items.insert(itemCount, event);
    notifyListeners();
  }

  int get itemCount {
    return _items.length;
  }

  // remove item from cart
  void removeItem(String eventId) {
    _items.forEach((element) {
      if (element.eventId == eventId) {
        _items.remove(element);
      }
    });
    notifyListeners();
  }

  void clear() {
    _items = [];
    notifyListeners();
  }
//gets all the events from firebase and place it to the events list
  Future<void> createEventList() async {
    clear();
    CollectionReference events = firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events');

    events.get().then((value) => value.docs.forEach((element) {
          SeatMeAppEvents event = SeatMeAppEvents();

          event.eventId = element.id;
          event.eventName = element.get('EventName');

          // DateFormat format = DateFormat("dd/MM/yyyy hh:mm");
          // Timestamp datetime = element.get('eventDate');
          // DateTime d = datetime.toDate();
          // var outputDate = format.format(d);
          event.eventDate = element.get('eventDate');

          addItem(event);
        }));
    notifyListeners();
  }

  void navigateToHomePage() {
            ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.homePage);
  }

  void navigateToEventDetailsPage() {
    ExtendedNavigator.named(RoutesNames.mainNav)
        .popAndPush(Routes.eventDetailsPage);
  }

//if we select an event - fetch the data from firebase
  Future<void> readObjectFromFirebase(String eventId) async {
    DocumentReference event = firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .doc(eventId);

    DocumentSnapshot ds = await event.get();
    currentEvent.eventName = ds.get('EventName');
    currentEvent.eventDate = ds.get('eventDate');
    currentEvent.numberOfGuests = ds.get('numberOfGuests');
    currentEvent.eventDate = ds.get('eventDate');
  }

  void navigateToSeatingPage(String eventId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt(HAVE_TABLES, 2);
    await updateActive(eventId);
    currentEvent.eventId = eventId;

    await readObjectFromFirebase(eventId);
    //ExtendedNavigator.named(RoutesNames.mainNav).push(Routes.loading);
    await ExtendedNavigator.named(RoutesNames.mainNav)
        .popAndPush(Routes.homePage);
  }

  int random(min, max) {
    var rn = new Random();
    return min + rn.nextInt(max - min);
  }


  updateActive(String eventId) async {
    DocumentReference event = firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .doc(eventId);

    if (currentEvent.eventId != null) {
      DocumentReference oldEvent = firestoreInstance
          .collection("Users")
          .doc(firebaseUser.uid)
          .collection('Events')
          .doc(currentEvent.eventId);

      await oldEvent.update({'active': false});
    }
    await event.update({'active': true});
  }



  Future<void> getCountOfConfirmations() async {
    var confirmed = firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId)
        .collection('Guests')
        .where('confirmed', isEqualTo: true);
    
    var notConfirmed = firestoreInstance
        .collection("Users")
        .doc(firebaseUser.uid)
        .collection('Events')
        .doc(currentEvent.eventId)
        .collection('Guests')
        .where('confirmed', isEqualTo: false);
    // var x = confirmed.get();
    // var y = notConfirmed.get();
    await confirmed.get().then((eventDetails) {
      currentEvent.numOfConfirmed = eventDetails.docs.length;
    });

    await notConfirmed.get().then((eventDetails) {
      currentEvent.numOfNotConfirmed = eventDetails.docs.length;
    });
    notifyListeners();
  }
}

void navigateToEventDetailsPage() {
  ExtendedNavigator.named(RoutesNames.mainNav)
      .popAndPush(Routes.eventDetailsPage);
}
