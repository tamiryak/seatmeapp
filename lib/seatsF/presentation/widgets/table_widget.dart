import 'package:flutter/material.dart';
import 'package:seatmeapp_finalproject/seatsF/presentation/widgets/seat_widget.dart';

//Image.asset("lib/core/assets/table.png"),

class TableWidget extends StatelessWidget {
  final width;
  final height;
  final String tableKey;
  final bool isPlus;

  TableWidget(this.width, this.height, this.tableKey, this.isPlus);

  @override
  Widget build(BuildContext context) {
//this is a build of table widget - created by 4 list of seats
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          !isPlus
              ? SizedBox(
                  height: height * 0.2,
                )
              : SizedBox(
                  width: 0,
                ),
          Container(
            height: height * 0.15,
            width: width * 0.7,
            // color: Colors.blue,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SeatWidget(tableKey: tableKey, seat: '1', isP: isPlus),
                SeatWidget(tableKey: tableKey, seat: '2', isP: isPlus),
                SeatWidget(tableKey: tableKey, seat: '3', isP: isPlus),
              ],
            ),
          ),
          Row(
            children: [
              !isPlus
                  ? SizedBox(
                      width: width * 0.15,
                    )
                  : SizedBox(
                      width: 0,
                    ),
              Container(
                width: width * 0.15,
                height: height * 0.7,
                // color: Colors.red,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SeatWidget(tableKey: tableKey, seat: '4', isP: isPlus),
                    SeatWidget(tableKey: tableKey, seat: '5', isP: isPlus),
                    SeatWidget(tableKey: tableKey, seat: '6', isP: isPlus),
                  ],
                ),
              ),
              Container(
                child:
                    Center(child:Text(tableKey.substring(16))),
                color: Color(0xfffff6de),
                height: height * 0.7,
                width: width * 0.7,
              ),
              Container(
                width: width * 0.15,
                height: height * 0.7,
                // color: Colors.red,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SeatWidget(tableKey: tableKey, seat: '7', isP: isPlus),
                    SeatWidget(tableKey: tableKey, seat: '8', isP: isPlus),
                    SeatWidget(tableKey: tableKey, seat: '9', isP: isPlus),
                  ],
                ),
              ),
            ],
          ),
          Container(
            height: height * 0.15,
            width: width * 0.7,
            // color: Colors.black,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SeatWidget(tableKey: tableKey, seat: '10', isP: isPlus),
                SeatWidget(tableKey: tableKey, seat: '11', isP: isPlus),
                SeatWidget(tableKey: tableKey, seat: '12', isP: isPlus),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
