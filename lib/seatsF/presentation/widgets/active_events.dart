import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:seatmeapp_finalproject/seatsF/presentation/state_managment/active_events_provider.dart';

class ActiveEvents extends StatelessWidget {
  final String id;
  final String eventId;
  final String date;
  final String name;

  const ActiveEvents({Key key , @required this.id, this.eventId, this.date, this.name});

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: ValueKey(id),
      background: Container(
        color: Theme.of(context).errorColor,
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40,
        ),
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: 20),
        margin: EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 4,
        ),
      ),
      direction: DismissDirection.endToStart,
      confirmDismiss: (direction) {
        return showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Are you sure?'),
            content: Text('Do you want to remove the item from the cart?'),
            actions: <Widget>[
              FlatButton(
                child: Text('No'),
                onPressed: () {
                  Navigator.of(ctx).pop(false);
                },
              ),
              FlatButton(
                child: Text('Yes'),
                onPressed: () {
                  Navigator.of(ctx).pop(true);
                },
              ),
            ],
          ),
        );
      },
      onDismissed: (direction) {
        Provider.of<EventListProvider>(context, listen: false)
            .removeItem(eventId);
      },
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
        child: Padding(
          padding: EdgeInsets.all(8),
          child: ListTile(
            leading: CircleAvatar(
              child: FittedBox(
                  // child: Text(('$date')),
                  ),
            ),
            title: Text('$name'),
            onTap: () {
              Provider.of<EventListProvider>(context, listen: false)
                  .navigateToSeatingPage(eventId);
            },
          ),
        ),
      ),
    );
  }
}
