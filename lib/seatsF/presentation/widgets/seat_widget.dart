import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart';
import 'package:seatmeapp_finalproject/core/entities/events.dart';
import 'package:seatmeapp_finalproject/core/entities/selected_guest.dart';
import 'package:seatmeapp_finalproject/routes/routes.gr.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';
import 'package:seatmeapp_finalproject/seatsF/presentation/state_managment/seat_widget_provider.dart';

class SeatWidget extends StatefulWidget {
  final String table;
  final String seatKey;
  final bool isPlus;

  SeatWidget({Key key, String tableKey, String seat, bool isP,var guest})
      : table = tableKey,
        seatKey = seat,
        isPlus = isP,super(key: key);

  @override
  _SeatWidgetState createState() => _SeatWidgetState();
}

class _SeatWidgetState extends State<SeatWidget> {
  final SeatMeAppEvents currentEvent = getIt();
  final SelectedGuest selectedGuest = getIt();
  var firebaseUser = FirebaseAuth.instance.currentUser;
  final FirebaseFirestore firestoreInstance = FirebaseFirestore.instance;

  String name;
  bool confirmed;
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: firestoreInstance
            .collection("Users")
            .doc(firebaseUser.uid)
            .collection('Events')
            .doc(currentEvent.eventId)
            .collection('Guests')
            .where('tableKey', isEqualTo: widget.table)
            .where('seat', isEqualTo: widget.seatKey)
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData || snapshot.data.docs.length == 0) {
            return widget.isPlus
                ? ClipOval(
                    child: Material(
                      color: Colors.brown[600], // button color
                      child: InkWell(
                        splashColor: Colors.white, // inkwell color
                        child: Center(
                            child: SizedBox(
                                width: 15,
                                height: 15,
                                child: Icon(
                                  Icons.add,
                                  size: 8,
                                  color: Colors.white70,
                                ))),
                        onTap: () {
                          // setState(() {});
                          currentEvent.tableSelected = widget.table; //if hall view - move to table seat page
                          ExtendedNavigator.named(RoutesNames.mainNav)
                              .push(Routes.tableSeatPage);
                        },
                      ),
                    ),
                  )
                : ClipOval(
                    child: Material(
                      color: Colors.brown[600], // button color
                      child: InkWell(
                        onTap: (){
                          Provider.of<SeatWidgetProvider>(context, listen: false)
            .saveGuestOnTableFireStore(selectedGuest,widget.seatKey,widget.table);//on pressing - save guest on table
                        },
                        child: Container(
                          width: 39,
                          height: 39,
                          child: Center(
                              child: Text("", style: TextStyle(fontSize: 5))),
                        ),
                      ),
                    ),
                  );
          } else {
            var userDocument = snapshot.data.docs[0];
            name = userDocument['guestName'];
            confirmed = userDocument['confirmed'];
            return Stack(
              alignment: Alignment.center,
              children: [
                ClipOval(
                  child: Material(
                    color: confirmed ? Colors.green : Colors.red, // button color
                    child: SizedBox(
                      width: widget.isPlus ? 15 : 39,
                      height: widget.isPlus ? 15 : 39,
                    ),
                  ),
                ),
                Center(
                          child: Text(name,textAlign: TextAlign.center,
                              style: TextStyle(fontSize: widget.isPlus ? 5 : 15,)))
              ],
            );
          }
        });
  }
}
