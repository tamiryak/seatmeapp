import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:seatmeapp_finalproject/guestsF/state_managment/guest_list_page_provider.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/state_managment/drawer_notifier.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/state_managment/login_provider.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/state_managment/new_event_provider.dart';
import 'package:seatmeapp_finalproject/loginF/presentation/widgets/charts_provider.dart';
import 'package:seatmeapp_finalproject/routes/routes.gr.dart';
import 'package:seatmeapp_finalproject/routes/routes_names.dart';
import 'package:seatmeapp_finalproject/seatsF/presentation/state_managment/active_events_provider.dart';
import 'package:seatmeapp_finalproject/guestsF/state_managment/guest_page_provider.dart';
import 'package:seatmeapp_finalproject/seatsF/presentation/state_managment/seating_page_provider.dart';

import 'guestsF/state_managment/update_guest_provider.dart';
import 'loginF/presentation/state_managment/home_page_provider.dart';
import 'seatsF/presentation/state_managment/seat_widget_provider.dart';

class SeatMeApp extends StatelessWidget {
  const SeatMeApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<LoginProvider>(create: (_) => LoginProvider()),
        ChangeNotifierProvider<NewEventPageProvider>(
            create: (_) => NewEventPageProvider()),
        ChangeNotifierProvider<SeatingPageProvider>(
            create: (_) => SeatingPageProvider()),
        ChangeNotifierProvider<HomePageProvider>(
            create: (_) => HomePageProvider()),
        ChangeNotifierProvider<GuestPageProvider>(
            create: (_) => GuestPageProvider()),
        ChangeNotifierProvider<EventListProvider>(
            create: (_) => EventListProvider()),
        ChangeNotifierProvider<GuestListPageProvider>(
            create: (_) => GuestListPageProvider()),
        ChangeNotifierProvider<DrawerNotifier>(
          create: (_) => DrawerNotifier(),
        ),
        ChangeNotifierProvider<ChartsProvider>(
          create: (_) => ChartsProvider(),
        ),
        ChangeNotifierProvider<SeatWidgetProvider>(
          create: (_) => SeatWidgetProvider(),
        ),
        ChangeNotifierProvider<UpdateGuestProvider>(
          create: (_) => UpdateGuestProvider(),
        ),
      ],
      child: MaterialApp(
        theme: ThemeData(fontFamily: 'SecularOne'),
        builder: ExtendedNavigator(
            router: SeatMeAppRouter(), name: RoutesNames.mainNav),
      ),
    );
  }
}
