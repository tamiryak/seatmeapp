import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:seatmeapp_finalproject/SeatMeApp.dart';
import 'package:seatmeapp_finalproject/core/di/di.dart' as di;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  di.setup();
  runApp(SeatMeApp());
}
